import { number2text } from "../helpers/convertEnglish.helper";

export const pdfTemplate = ({ 
    orderItemId, 
    invoiceNumber,  
    invoiceDate,
    name, 
    mobileNumber,
    street, 
    city, 
    state,
    pincode,
    txnId,
    paymentMode,
    paymentDateTime,
    productDescription,
    productUnitPrice,
    productQty,
}:any):string => {
    function capitalize(str:any) {
        const lowerCaseString = str.toLowerCase();
        const firstLetter = str.charAt(0).toUpperCase();
        const strWithoutFirstChar = lowerCaseString.slice(1); 
        return firstLetter + strWithoutFirstChar;         
    }
    return `
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PDF Invoice Template</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <style>
        *{
            font-family: arial;
        }
        body{
            margin-left: auto;
            margin-right: auto;
        }
        .invoice_container{
            width: 100%;
            position: relative; 
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica';
            color: #555;
        }
        .invoice_header{
            display: flex;
            border: 1px solid rgb(131, 126, 126);
            margin: 10px 10px -1px 10px;
            position: relative; 
            padding: 10px;
        }
        .logo_container{
            margin-top: auto;
            margin-bottom: auto;
            margin-left: 40px;
            object-fit: cover;
        }
        .company_address{
            margin-left: 50px;
        }
        .invoice_name{
            margin: 0;
            position: absolute;
            bottom: 0;
            right: 0;
            padding: 10px;
        }
        .invoice_header h2{
            margin-bottom: 0;
        }
        .invoice_header p{
            margin-top: 10px;
        }
        .logo_container img{
            height: 60px;
        }
        .customer_container{
            margin: -1px 10px -1px 10px;
            display: flex;
            border: 1px solid rgb(131, 126, 126);
        }
        .customer_container div{
            width: 50%;
            padding: 10px;
        }
        .customer_container .details{
            border-right: .5px solid rgb(131, 126, 126);
        }
        .customer_container h2{
            margin-bottom: 5px;
            margin-top: 5px;  
        }
        .customer_container h4{
            margin-bottom: 0px;
            margin-top: 0;
        }
        .customer_container p{
            margin: 0;
        }
        .product_container{
            padding: 0 10px;
            margin: -1px 0px -1px 0px;
        }
        .item_table{
            width: 100%;
            position: relative; 
            text-align: left; 
            border-collapse: collapse;
            border: .02em solid black;
        }
        .item_table td, .item_table th{
            padding: 5px 10px;
        }
        .item_table td, .item_table th {
            border: .02em solid rgb(131, 126, 126);
        }
        .invoice_footer{
            padding: 20px;
            margin: -1px 10px -1px 10px;
            display: flex;
            justify-content: space-between;
            border: 1px solid rgb(131, 126, 126);
        }
        .note{
            padding: 5px;
            margin: -1px 10px -1px 10px;
            border: 1px solid rgb(131, 126, 126);
        }
        .note h2, h3{
            padding: 2px;
            margin: 0;
        }
        .auth_container{
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .auth_container img{
            width:150px;
            object-fit: cover;
        }
        </style>
    </head>
    <body>
        <div class="invoice_container">
            <div class="invoice_header">
                <div class="logo_container">
                    <img src="https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/shop-logo/logo.png" alt="logo">
                </div>
                <div class="company_address">
                    <h2>Urban Plants™</h2> 
                    <p>
                        D-701/7TH FLAT, SECT-5,<br> 
                        SHRI GANINATH NIKUNJ, CGHS DWARKA,<br>
                        DELHI South West Delhi DL 110075,<br>
                        INDIA
                    </p>
                </div>
                <h1 class="invoice_name">TAX INVOICE</h1>
            </div>
            <div class="customer_container">
                <div class="details">
                    <table>
                        <tr>
                            <td>Invoice No</td>
                            <td>:</td>
                            <td><b>${invoiceNumber}</b></td>
                        </tr>
                        <tr>
                            <td>Invoice Date</td>
                            <td>:</td>
                            <td><b>${invoiceDate}</b></td>
                        </tr>
                        <tr>
                            <td>Order Number</td>
                            <td>:</td>
                            <td><b>${orderItemId}</b></td>
                        </tr>
                        <tr>
                            <td>Order Date</td>
                            <td>:</td>
                            <td><b>${invoiceDate}</b></td>
                        </tr>
                        <tr>
                            <td>Place of Supply</td>
                            <td>:</td>
                            <td><b>${capitalize(state)}</b></td>
                        </tr>
                    </table>
                </div>
                <div>
                    <h2>Billing To</h2>
                    <h4>${capitalize(name)}</h4>
                    <p>
                        ${street},${capitalize(city)}<br>
                        ${capitalize(state)}, ${pincode}
                    </p>
                    <p><b>${mobileNumber}</b></p>
                </div>
            </div>    
            <div class="product_container">
                <table class="item_table">
                    <tr>
                        <th style="width:4%">Sl. No.</th>
                        <th style="width:50%">Description</th>
                        <th style="width:10%">Unit Price</th>
                        <th style="width:5%">Qty</th>
                        <th style="width:10%">Net Amount</th>
                        <th style="width:3%">Tax Rate</th>
                        <th style="width:3%">Tax Type</th>
                        <th style="width:5%">Tax Amount</th>
                        <th style="width:10%">Total Amount</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>${productDescription}</td>
                        <td>${productUnitPrice}</td>
                        <td>${productQty}</td>
                        <td>40,000</td>
                        <td>18%</td>
                        <td>IGST</td>
                        <td>2000</td>
                        <td>${Number(productUnitPrice) * Number(productQty)}</td>
                    </tr>
                    <tr>
                        <th colspan="7">Total</th>
                        <th>2000</th>
                        <th>${Number(productUnitPrice) * Number(productQty)}</th>
                    </tr>
                </table>
            </div>
            <div class="note">
                <h2>Amount in Words:</h2>
                <p>${number2text(Number(productUnitPrice) * Number(productQty))}</p>
            </div>
            <div class="invoice_footer">
                <div>
                    <div>
                        <table>
                            <tr>
                                <td>Payment Transaction ID</td>
                                <td>:</td>
                                <td><b>${txnId}</b></td>
                            </tr>
                            <tr>
                                <td>Date & Time</td>
                                <td>:</td>
                                <td><b>${paymentDateTime} hrs</b></td>
                            </tr>
                            <tr>
                                <td>Invoice Value</td>
                                <td>:</td>
                                <td><b>${Number(productUnitPrice) * Number(productQty)}</b></td>
                            </tr>
                            <tr>
                                <td>Mode of Payment</td>
                                <td>:</td>
                                <td><b>${paymentMode}</b></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="auth_container">
                    <p>Atul Dubey</p>
                    <img src="https://rxxhfsxstrznedzpsgzb.supabase.co/storage/v1/object/public/image/shop-logo/AuthorizedSignature.jpg" alt="signature">
                    <p>Authorized Signatory</p>
                </div>
            </div>
        </div>
    </body>
    </html>     
    `
};
