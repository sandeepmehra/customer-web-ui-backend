import express from 'express';
import logger from '../../config/logger.config';
import { getOrderDetailService, getOrderInvoiceService, getOrdersService, paymentServiceCallback, placeOrderService } from '../services/order.service';
require('dotenv').config();

export const getOrdersController = async (req: express.Request, res: express.Response) => {
    const data = await getOrdersService(req.email, req.mobile);
    // console.log("data.orders: ", data.orders);
    if (data.status === 500) {
        logger('error', 'GET', '/orders', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({"orders": data.orders});
}


export const getOrderDetailController = async (req: express.Request, res: express.Response) => {
    const data = await getOrderDetailService(Number(req.params.order_item), req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', `/order/${req.params.order_item}`, 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json( data.order);
}

export const placeOrderController = async (req: express.Request, res: express.Response) => {
    const data:any = await placeOrderService(req.body, req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'POST', `/place-order`, 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.params);
}

export const placeOrderCallbackController = async (req: express.Request, res: express.Response) => {
    await paymentServiceCallback(req, res);
}

export const getInvoiceController = async (req: express.Request, res: express.Response) => {
    await getOrderInvoiceService(Number(req.params.order_item), req, res, req.email, req.mobile);
}
