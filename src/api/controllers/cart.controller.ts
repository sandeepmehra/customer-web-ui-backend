import express from 'express';
import logger from '../../config/logger.config';
import { putAddCartService, getCartService, postCartService, putSubCartService, removeCartService } from '../services/cart.service';
require('dotenv').config();

export const getCartController = async (req: express.Request, res: express.Response) => {
    const data = await getCartService(req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'GET', '/cart', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({"cart":data.cart});
}

export const postCartController = async (req: express.Request, res: express.Response) => {
    const data = await postCartService(Number(req.params.product_variant_id), req.body, req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'POST', '/cart/:product_variant_id', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({});
}

export const putAddCartController = async (req: express.Request, res: express.Response) => {
    const data = await putAddCartService(Number(req.params.product_variant_id), req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'PUT', '/cart/:product_variant_id/add', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const putSubCartController = async (req: express.Request, res: express.Response) => {
    const data = await putSubCartService(Number(req.params.product_variant_id), req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'PUT', '/cart/:product_variant_id/sub', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const removeCartController = async (req: express.Request, res: express.Response) => {
    const data = await removeCartService(Number(req.params.product_variant_id), req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'POST', '/cart/remove', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({});
}