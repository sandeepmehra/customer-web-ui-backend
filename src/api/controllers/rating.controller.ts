import express from 'express';
import logger from '../../config/logger.config';
import { postImageService, postRatingReviewImageService, postRatingReviewService } from "../services/rating.service";
require('dotenv').config();

    
// export const postRatingReviewController = async (req: express.Request, res: express.Response) => {
//     const data = await postRatingReviewService(Number(req.params.order_item_id), req.body, req.email, req.mobile);
//     if(data.status === 500){
//         logger('error', 'POST', `/review/${req.params.order_item_id}`, 500, data.message);
//         return res.status(data.status).json({
//             "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
//         });
//     }
//     return res.status(data.status).json({});
// }

export const postImageController = async (req:express.Request, res: express.Response) => {
    await postImageService(Number(req.params.order_item_id), req, res, req.email, req.mobile);       
}

export const postRatingReviewController = async (req: express.Request, res: express.Response) => {
    const data:any = await postRatingReviewImageService(Number(req.params.order_item_id), req, res, req.email, req.mobile);
    // console.log("data: ", data);
    // if(data.status === 500 || data.status === 400 || data.status === 401 || data.status !== 'aslfskd'){
    //     logger('error', 'POST', `/review/${req.params.order_item_id}`, data.status, data.message);
    //     return res.status(data.status).json({
    //         "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
    //     });
    // }
    // return res.status(data.status).json({});
}
    