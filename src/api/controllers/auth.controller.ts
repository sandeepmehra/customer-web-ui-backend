import express from 'express';
import logger from '../../config/logger.config';
import { googleLoginService, loginWithEmail, otpEmailVerifyService, otpVerifyService, passwordResetService, refresh, signupWithEmail, storeEmailOrOTP, storeNumberOrOTP } from '../services/auth.service';
import { userExistsCheckByEmail } from '../validations/auth.validation';
require('dotenv').config();


export async function loginController(req: express.Request, res: express.Response) {
    const tokens = await loginWithEmail(req.body);
    if (tokens.status === 403){
        logger('error', 'POST', '/login', 403, "Account suspended");
        return res.status(403).json({ "message":  "Account suspended"});
    }
    if (tokens.access_token === undefined) {
        logger('error', 'POST', '/login', 401, "Access token undefined");
        return res.status(401).json({ "message": process.env.RESPONSE_UNAUTHORIZED });
    }
    return res
        .status(200)
        .setHeader('Authorization', tokens.access_token)
        .json({ "refresh_token": tokens.refresh_token });
}

export async function loginWithMobileController(req: express.Request, res: express.Response) {
    const data: any =  await storeNumberOrOTP(req.body.mobile_number);
    if(data.status !== undefined && data.status === 401){
        logger('error', 'POST', '/login/mobile', 401, "Wati token error");
        return res.status(500).json({ "message": "Not able to send OTP please try again /login/signup methods." });
    }
    if(data.status !== undefined && data.status === 400){
        logger('error', 'POST', '/login/mobile', 400, data.data);
        return res.status(400).json({ "message": 'Invalid Whatsapp Number' });
    }
    return res
        .status(data.status)
        .json({});
}

export async function verifyMobileOtpController(req: express.Request, res: express.Response){
    const tokens: any = await otpVerifyService(req.body);
    if (tokens.status === 400 || tokens.status === 500) {
        logger('error', 'POST', '/verify/phone', tokens.status, tokens.message);
        return res.status(tokens.status).json({ "message": tokens.status === 400 ? tokens.message : process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    if (tokens.access_token === undefined) {
        logger('error', 'POST', '/verify/phone', 401, "Access token undefined");
        return res.status(401).json({ "message": process.env.RESPONSE_UNAUTHORIZED });
    }
    return res
        .status(200)
        .setHeader('Authorization',tokens.access_token)
        .json({ "refresh_token": tokens.refresh_token });
}

export async function loginResetController(req: express.Request, res: express.Response) {
    const data =  await storeEmailOrOTP(req.body.email);
    if (data.status === 400 || data.status === 500) {
        logger('error', 'POST', '/password/reset', data.status, 'Error in loginResetController');
        return res.status(data.status).json({ "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res
        .status(data.status)
        .json(data.data);
}

export async function verifyEmailOtpController(req: express.Request, res: express.Response){
    const data: any = await otpEmailVerifyService(req.body);
    
    if (data.status === 400 || data.status === 500) {
        logger('error', 'POST', '/verify/phone', data.status, data.message);
        return res.status(data.status).json({ "message": data.status === 400 ? data.message : process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    
    return res.status(200).json({});
}

export async function signupController(req: express.Request, res: express.Response) {
    const val_data = await userExistsCheckByEmail(req.body); 
    if (!(val_data.status)) {
        logger('error', 'POST', '/signup/email', val_data.status_code, val_data.log_message);
        return res.status(val_data.status_code).json({
            "message": val_data.message
        });
    }
    console.log("signup with Email controller", req.body);
    const data = await signupWithEmail(req.body);
    if (data.status === 500) {
        logger('error', 'POST', '/signup/email', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message); 
}

export async function passwordResetController(req: express.Request, res: express.Response) {
    const data = await passwordResetService( req.body, req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'PUT', '/password', data.status, data.message);
        return res.status(data.status).json({ "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res
        .status(data.status)
        .json({})
}

export async function refreshController(req: express.Request, res: express.Response) {
     await refresh(req, res); 
}

export async function googleLoginController(req: express.Request, res: express.Response) {
    const token = await googleLoginService(req.user);
    if(token !== undefined && token.status === 401){
        logger('error', 'GET', '/auth/login/google/success', 401, "Google Access token undefined");
        return res.status(401).json({ "message": process.env.RESPONSE_UNAUTHORIZED });
    }
    if (token.status === 500) {
        logger('error', 'GET', '/auth/login/google/success', 500, "Google Access token undefined");
        return res.status(500).json({ "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res
        .status(200)
        .setHeader('Authorization', token.at as string)
        .json({"refresh_token": token.rt as string})
}

