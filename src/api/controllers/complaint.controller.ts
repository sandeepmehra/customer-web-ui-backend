import express from 'express';
import logger from '../../config/logger.config';
import { getByIdComplaintService, getComplaintService, postComplaintResponseService, postComplaintService } from '../services/complaint.service';
require('dotenv').config();


export const getComplaintController = async (req: express.Request, res: express.Response) => {
    const data = await getComplaintService(req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'GET', '/complaints', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.complaints);
}

export const getByIdComplaintController = async (req: express.Request, res: express.Response) => {
    const data = await getByIdComplaintService(Number(req.params.complaint_id), req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', `/complaint/${req.params.complaint_id}`, 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.complaint);
}

export const postComplaintController = async (req: express.Request, res: express.Response) => {
    const data = await postComplaintService(req.body, req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'POST', '/complaint', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({});
}

export const postComplaintResponseController = async (req: express.Request, res: express.Response) => {
    const data = await postComplaintResponseService(Number(req.params.complaint_id), req.body, req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'POST', '/response/:complaint_id', 500, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json({});
}



