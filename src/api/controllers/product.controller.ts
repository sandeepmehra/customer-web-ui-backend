import express from 'express';
import logger from '../../config/logger.config';
import { categoryAlsoLikeService, categoryService, getAllCategoryService, productService, productsService, specificCategoryService } from '../services/product.service';
require('dotenv').config();

export const productController = async (req: express.Request, res: express.Response) => {
    const data = await productService(Number(req.params.product_id));
    if(data.status === 500){
        logger('error', 'GET', `/product/${req.params.product_id}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product);
}

export const categoryController = async (req: express.Request, res: express.Response) => {
    const data = await categoryService(String(req.params.category), req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', `/products/category/${req.params.category}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product_category_detail);
}
export const categoryAlsoLikeController = async (req: express.Request, res: express.Response) => {
    const data = await categoryAlsoLikeService(String(req.params.category));
    if(data.status === 500){
        logger('error', 'GET', `/products/category/alsolike/${req.params.category}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product_category_detail);
}
export const getAllCategoryController = async (req: express.Request, res: express.Response) => {
    const data = await getAllCategoryService();
    if(data.status === 500){
        logger('error', 'GET', '/products/category', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({"category_list":data.product_category_list});
}

export const productsController = async (req: express.Request, res: express.Response) => {
    const data = await productsService();
    if(data.status === 500){
        logger('error', 'GET', '/product', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "product": data.product,
    });
}

export const categoryPlantsController = async (req: express.Request, res: express.Response) => {
    const data = await specificCategoryService(String(`Plants`), Number(req.params.limit));
    if(data.status === 500){
        logger('error', 'GET', `/products/category/Plants/${Number(req.params.limit)}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product_category_detail);
}
export const categoryPotsController = async (req: express.Request, res: express.Response) => {
    const data = await specificCategoryService(String(`Pots`), Number(req.params.limit));
    if(data.status === 500){
        logger('error', 'GET', `/products/category/Pots/${Number(req.params.limit)}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product_category_detail);
}
export const categoryToolsController = async (req: express.Request, res: express.Response) => {
    const data = await specificCategoryService(String(`Accessories`), Number(req.params.limit));
    if(data.status === 500){
        logger('error', 'GET', `/products/category/Tools/${Number(req.params.limit)}`, 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json(data.product_category_detail);
}


