import express from 'express';
import logger from '../../config/logger.config';
import { addressInfoPostService, addressInfoUpdateService, addToWishlistService, basicInfoService, getUserAddressService, getUserInfoService, getWishlistService, removeToWishlistService, } from '../services/user.service';
require('dotenv').config();


export const updateInfoController = async(req: express.Request, res: express.Response) => {
    const data:any = await basicInfoService(req.body, req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'PUT', '/account', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }    
    if (data.access_token === undefined) {
        logger('error', 'PUT', '/account', 401, "Access token undefined");
        return res.status(401).json({ "message": process.env.RESPONSE_UNAUTHORIZED });
    }
    return res
        .status(200)
        .setHeader('Authorization', data.access_token)
        .json({ "refresh_token": data.refresh_token });
}

export const getUserInfoController = async (req: express.Request, res: express.Response) => {
    const data = await getUserInfoService(req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', '/account', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "user_info": data.userInfo,
    });
}

export const getUserAddressController = async (req: express.Request, res: express.Response) => {
    const data = await getUserAddressService(req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', '/account/address', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "address_info": data.userAddress,
    });
}

export const addressInfoPostController = async(req: express.Request, res: express.Response) => {
    const data = await addressInfoPostService(req.body, req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'POST', '/account/address', data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const addressInfoUpdateController = async(req: express.Request, res: express.Response) => {
    const data = await addressInfoUpdateService(req.body, Number(req.params.customer_id), req.email, req.mobile );
    if (data.status === 500) {
        logger('error', 'PUT', `/account/${req.params.customer_id}/address`, data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}


export const getWishlistController = async (req: express.Request, res: express.Response) => {
    const data = await getWishlistService(req.email, req.mobile);
    if(data.status === 500){
        logger('error', 'GET', '/wishlist', 500, data.message);
        return res.status(data.status).json({"message": process.env.RESPONSE_INTERNAL_SERVER_ERROR});
    }
    return res.status(data.status).json({
        "wishlist": data.wishlist,
    });
}

export const addToWishlistController = async(req: express.Request, res: express.Response) => {
    const data = await addToWishlistService(Number(req.params.product_variant_id), req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'POST', `/wishlist/${req.params.product_variant_id}`, data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}

export const removeToWishlistController = async(req: express.Request, res: express.Response) => {
    const data = await removeToWishlistService(Number(req.params.wishlist_id), req.email, req.mobile);
    if (data.status === 500) {
        logger('error', 'DELETE', `/wishlist/${req.params.wishlist_id}`, data.status, data.message);
        return res.status(data.status).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
    return res.status(data.status).json(data.message);
}