export interface LoginNetworkDto {
    name: string;
    email: string;
    password: string;
}