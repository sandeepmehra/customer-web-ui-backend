export interface ordersResponseDto{
    id: string;
    product_name: string;
    total_price: string;
    quantity: string;
    status_text: string;
    shop_name: string;
    image: string;
}
export interface orderResponseDto{
    id: string;
    order_id: string;
    product_name: string;
    total_price: string;
    quantity: string;
    status_text: string;
    shop_name: string;
    image: string;
    address: orderAddressResponseDto;
    review_action_taken: boolean | null;
}
export interface orderAddressResponseDto {
    name: string;
    mobile_number: string;
    city: string;
    street: string;
    state: string;
    pincode: string;
}
export interface orderAddressDbDto {
    customer_order_address:{
        name: string;
        mobile_number: string;
        address: {
            city: string;
            street: string;
            state: string;
            pincode: string;  
        }
    }  
}
export interface orderDBDto{
    id: number | bigint;
    quantity: number;
    price_per_unit: number;
    status: string;
    product_variant: {
        product_variant_images: [{
            url: string
        }];
        product: {
            name: string;
            shop: {
                name: string;
            }
        }
    }
}