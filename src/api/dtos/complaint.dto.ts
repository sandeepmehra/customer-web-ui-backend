export interface complaintResponsePostDto {
    complaint_id: string;
    response: string;
    email: string;
}
export interface complaintsResponseDto{
    id: string;
    subject: string;
    description: string;
    open: boolean;
    created_on: string;
    closed_on?: string;
    responses?: responsesArrayDto[] | undefined
}
export interface complaintsDBDto{
    id: string;
    subject: string;
    description: string;
    open: boolean;
    created_on: string;
    closed_on: string;
    customer_response?: [{
        id: string;
        response: string;
        create_on: string;
    }];
    admin_response?: [{
        id: string;
        response: string;
        created_on: string;
        user_admin: {
            name: string;
        }
    }]
}

export interface combineArrayDto {
    from?: {
        user_admin?:{
            name: string 
        }
    };
    created_on: string;
    response: string
}
export interface responsesArrayDto {
    from?: string;
    datetime: string;
    response: string
}

export interface complaintsStatusResponseDto {
    id: string;
    subject: string;
    created_on?: string;
    closed_on?: string;
}

export interface complaintsStatusDBDto {
    id: string;
    subject: string;
    created_on?: string;
    closed_on?: string;
    open: boolean;
}

export interface complaintsDto{
    in_progress?: complaintsStatusResponseDto[] | undefined;
    resolved?: complaintsStatusResponseDto[] | undefined;
}