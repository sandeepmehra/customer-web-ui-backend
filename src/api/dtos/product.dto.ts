export interface productVariantResponseDto{
    price: string;
    discounted_price: string;
    sku: string;
    country_of_origin: string;
    color: string;
    measurements: string; 
    images: Array<string>;
    videos: Array<string>;
}
export interface productResponseDto{
    product:object;
    product_variants: object;
}
export interface productInfoDto{
    product_name: string;
    description: string;
    category: string;
    shop_name: string;
    colors_available: object;
    sizes_available: object;
}