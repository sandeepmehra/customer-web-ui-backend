export interface cartResponseDto{
    id: string;
    product_variant_id: string,
    quantity: string;
    total_price: string;
    product_name: string;
    product_id: string;
    shop_name: string;
    image: string;  
    price: string;
    discounted_price: string;
}

export interface cartDbDto{
    id: bigint | number;
    quantity: number;
    product_variant: {
        id: bigint | number;
        price: number| bigint| string;
        discounted_price: number | string;
        product_variant_images:[{
            url: string
        }];
        product:{
            id: bigint;
            name: string;
            shop: {
                name: string;
            }
        }
    }
}