export interface wishlistResponseDto{
    id: string;
    product_variant_id: string,
    product_name: string;
    description: string;
    price: number;
    discounted_price: number;
    shop_name: string;
    image: string;  
}

export interface wishlistDbDto{
    id: bigint | number;
    product_variant: {
        id: bigint | number;
        price: number;
        discounted_price: number;
        product_variant_images:[{
            url: string
        }];
        product:{
            name: string;
            description: string;
            shop: {
                name: string;
            }
        }
    }
}