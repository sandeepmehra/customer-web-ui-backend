export interface userAddressBodyDto{
    name?: string; 
    mobile_number?: string; 
    address?: {
        city?: string; 
        street?: string; 
        state?: string; 
        pincode?: number;
    };
    city?: string; 
    street?: string; 
    state?: string; 
    pincode?: number;
}
export interface userPutAddressDto{
    name?: string; 
    mobile_number?: string; 
    address?: {
        city?: string; 
        street?: string; 
        state?: string; 
        pincode?: number;
    };
    city?: string; 
    street?: string; 
    state?: string; 
    pincode?: number;
}
export interface userAddressDto{
    name: string;
    mobile_number: string | '';
    city: string;
    street: string;
    state: string;
    pincode: string;    
}
export interface userAddressResponseDto{
    id: string;
    name: string;
    mobile_number: string;
    city: string;
    street: string;
    state: string;
    pincode: string;    
}
export interface userAddressDBDto{
    customer_order_address:{
        name: string;
        mobile_number: string;
        address: {
            city: string;
            street: string;
            state: string;
            pincode: string;  
        }
    }  
}