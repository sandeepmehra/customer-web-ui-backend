import validator from 'validator';
import prisma from "../../../prisma/prisma-client";
import logger from "../../config/logger.config";
import { LoginNetworkDto } from '../dtos/auth.dto';
require('dotenv').config();

const isJWT = (value: string) => value.split('.').length === 3 ? true : false

export const isAuthHeaderValid = (value: string | undefined): boolean =>
    value !== undefined && !validator.isEmpty(value) && ((value: string) => value.split(' ').length == 2 ? true : false) && (value.split(' ')[0] === 'Bearer' ? true : false) && isJWT(value.split(' ')[1]);

export const isRefreshTokenBodyValid = (value: string): boolean => !validator.isEmpty(value) && isJWT(value);


export const userExistsCheckByEmail = async (userDto: LoginNetworkDto ): Promise < {
    status: boolean,
    status_code: number,
    message: string | undefined,
    log_message: string
} > => {
    try {
        const userExists = await prisma.user_customer.findUnique({
            where:{
                email: userDto.email
            },
            select: {
                email: true
            }
        });
        return {
            status: userExists?.email !== userDto.email ? true: false,
            status_code: userExists?.email !== userDto.email ? 200 : 400,
            message: 'Please provide valid User',
            log_message: 'Provided email already present in database'
        };
    } catch (error: any) {
        return {
            status: false,
            status_code: 500,
            message: process.env.RESPONSE_INTERNAL_SERVER_ERROR,
            log_message: error.message
        };
    }
}
