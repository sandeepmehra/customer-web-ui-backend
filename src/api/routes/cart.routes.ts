import express from 'express';
import { body, param } from 'express-validator';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import {
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';
import { getCartController, postCartController, putAddCartController, putSubCartController, removeCartController } from '../controllers/cart.controller';


export class CartRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'CartRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/cart', [
            accessTokenPrivateVerificationMiddleware,
            getCartController
        ]);

        this.app.post('/cart/:product_variant_id', [
            accessTokenPrivateVerificationMiddleware,
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('quantity').notEmpty().withMessage('Please enter a valid quantity').bail()
            .isString().withMessage('Please enter a valid quantity'),
            SingleBodyValidationMiddleware,
            postCartController
        ]);

        this.app.put("/cart/:product_variant_id/add", [
            accessTokenPrivateVerificationMiddleware,
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,  
            putAddCartController 
        ]);

        this.app.put("/cart/:product_variant_id/sub", [
            accessTokenPrivateVerificationMiddleware,
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,  
            putSubCartController
        ]);

        this.app.delete('/cart/removeItem/:product_variant_id', [
            accessTokenPrivateVerificationMiddleware,
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            removeCartController
        ]);
        return this.app;
    }
}