import express from 'express';
import { body, header} from 'express-validator';
import { isAuthHeaderValid, isRefreshTokenBodyValid} from '../validations/auth.validation';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import { loginController, refreshController, signupController, loginWithMobileController, verifyMobileOtpController, loginResetController, verifyEmailOtpController, googleLoginController, passwordResetController } from '../controllers/auth.controller';
import passport from 'passport';
import { accessTokenPrivateVerificationMiddleware } from '../middlewares/auth.middleware';


export class AuthRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'AuthRoutes');
    }
    configureRoutes(): express.Application {
        this.app.post('/login', [
            body('email').isEmail().withMessage('Please enter a valid Email'),
            SingleBodyValidationMiddleware,
            body('password').notEmpty().withMessage('Please enter a valid Password').bail().isString().withMessage('Please enter a valid Password').bail()
            .isLength({ min: 6 }).withMessage('Password should be atleast 6 character long'),
            SingleBodyValidationMiddleware,
            loginController
        ]);
        
        this.app.post('/auth/mobile', [
            body('mobile_number').notEmpty().withMessage('Please enter a valid Mobile Number').bail()
            .isString().withMessage('Please enter a valid Mobile Number').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid Mobile Number'),
            SingleBodyValidationMiddleware,
            loginWithMobileController
        ]);

        this.app.post('/verify/otp',[
            body('mobile_number').notEmpty().withMessage('Please enter a valid Mobile Number').bail()
            .isString().withMessage('Please enter a valid Mobile Number').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid Mobile Number'),
            SingleBodyValidationMiddleware,
            body('otp').notEmpty().withMessage('Please enter a valid OTP').bail()
            .isString().withMessage('Please enter a valid OTP'),
            SingleBodyValidationMiddleware,
            verifyMobileOtpController
        ]);
        
        this.app.put('/password/reset', [
          body('email').isEmail().withMessage('Please enter a valid Email'),
          SingleBodyValidationMiddleware,
          loginResetController
        ]);

        this.app.put('/verify/email',[
          body('email').isEmail().withMessage('Please enter a valid Email'),
          SingleBodyValidationMiddleware,
          body('password').notEmpty().withMessage('Please enter a valid Password').bail().isString().withMessage('Please enter a valid Password').bail()
          .isLength({ min: 6 }).withMessage('Password should be atleast 6 character long'),
          SingleBodyValidationMiddleware,
          body('otp').notEmpty().withMessage('Please enter a valid OTP').bail()
          .isString().withMessage('Please enter a valid OTP'),
          SingleBodyValidationMiddleware,
          verifyEmailOtpController
        ]);

        this.app.post('/signup/email', [
            body('name').notEmpty().withMessage('Please enter a valid Name').bail()
            .isString().withMessage('Please enter a valid Name'),
            SingleBodyValidationMiddleware,
            body('email').isEmail().withMessage('Please enter a valid Email'),
            SingleBodyValidationMiddleware,
            body('password').notEmpty().withMessage('Please enter a valid Password').bail()
            .isString().withMessage('Please enter a valid Password').bail()
            .isLength({ min: 6 }).withMessage('Password should be atleast 6 character long'),
            SingleBodyValidationMiddleware,
            signupController
        ]);

        this.app.put('/password', [
          accessTokenPrivateVerificationMiddleware,
          body('password').notEmpty().withMessage('Please enter a valid Password').bail().isString().withMessage('Please enter a valid Password').bail()
          .isLength({ min: 6 }).withMessage('Password should be atleast 6 character long'),
          SingleBodyValidationMiddleware,
          passwordResetController
        ]);

        this.app.post('/refresh', [
          header('authorization').custom(isAuthHeaderValid).withMessage('Please send a valid access token'),
          SingleBodyValidationMiddleware,
          body('refresh_token').custom(isRefreshTokenBodyValid).withMessage('Please send a valid refresh token'),
          SingleBodyValidationMiddleware,
          header('authorization').customSanitizer((token: string)=> token.split(' ')[1]),
          refreshController
        ]);

        this.app.get("/auth/login/google/success", googleLoginController);

        this.app.get("/auth/login/failed", (req, res) => {
            res.status(401).json({
              success: false,
              message: "failure",
            });
          });
          
        
        
          this.app.get('/auth/google', passport.authenticate("google", { scope: ["profile", "email"], prompt: 'consent' }) );
          

          this.app.get(
              "/auth/google/callback",
              passport.authenticate("google", {
                successRedirect: `${process.env.UI_URL}/auth/google/success`,
                failureRedirect: "/auth/login/failed",
              })
          );

        return this.app;
    }
}