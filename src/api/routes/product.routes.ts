import express from 'express';
import {
    header,
    param
} from 'express-validator';
import {
    SingleBodyValidationMiddleware
} from "../middlewares/common.middleware";
import {
    RoutesConfig
} from "../../config/routes.config";
import {
    accessTokenProtectedVerificationMiddleware,
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';
import {
    categoryAlsoLikeController,
    categoryController,
    categoryPlantsController,
    categoryPotsController,
    categoryToolsController,
    getAllCategoryController,
    productController,
    productsController
} from '../controllers/product.controller';


export class ProductRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'ProductRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/product/:product_id', [
            param('product_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid product id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            productController
        ]);

        this.app.get('/products/category/:category', [
            accessTokenProtectedVerificationMiddleware,
            param('category').isString().withMessage('Please enter a valid category'),
            SingleBodyValidationMiddleware,
            categoryController
        ]);
        this.app.get('/products/category/alsolike/:category', [
            param('category').isString().withMessage('Please enter a valid category'),
            SingleBodyValidationMiddleware,
            categoryAlsoLikeController
        ]);
        this.app.get('/products/category', getAllCategoryController);

        this.app.get('/products', [
            accessTokenPrivateVerificationMiddleware,
            productsController
        ]);
        this.app.get('/products/category/Plants/:limit', [
            param('limit').isString().withMessage('Please enter a valid limit'),
            SingleBodyValidationMiddleware,
            categoryPlantsController
        ]);
        this.app.get('/products/category/Pots/:limit', [
            param('limit').isString().withMessage('Please enter a valid limit'),
            SingleBodyValidationMiddleware,
            categoryPotsController
        ]);
        this.app.get('/products/category/Accessories/:limit', [
            param('limit').isString().withMessage('Please enter a valid limit'),
            SingleBodyValidationMiddleware,
            categoryToolsController
        ]);
        return this.app;
    }
}