import express from 'express';
import { param } from 'express-validator';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import {
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';
import { getInvoiceController, getOrderDetailController, getOrdersController, placeOrderCallbackController, placeOrderController } from '../controllers/order.controller';


export class OrderRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'OrderRoutes');
    } 
    configureRoutes(): express.Application {
        this.app.get('/orders', [
            accessTokenPrivateVerificationMiddleware,
            getOrdersController
        ]);

        this.app.get('/order/:order_item', [
            accessTokenPrivateVerificationMiddleware,
            param('order_item').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            getOrderDetailController
        ]);

        this.app.post('/place-order', [
            accessTokenPrivateVerificationMiddleware,
            placeOrderController
        ]);
        
        this.app.post('/place-order/callback', [
            placeOrderCallbackController
        ]);

        this.app.get('/order/:order_item/payment/invoice/download', [
            accessTokenPrivateVerificationMiddleware,
            param('order_item').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            getInvoiceController
        ]);
        
        return this.app;
    }
}