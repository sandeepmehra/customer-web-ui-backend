import express from 'express';
import { body, param } from 'express-validator';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import {
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';
import { postImageController, postRatingReviewController } from '../controllers/rating.controller';


export class RatingRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'RatingRoutes');
    }
    configureRoutes(): express.Application {
        // this.app.post('/review/:order_item_id', [
        //     accessTokenVerificationMiddleware,
        //     param('order_item_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
        //     .toInt(),
        //     SingleBodyValidationMiddleware,
        //     body('review').notEmpty().withMessage('Please enter a valid review').bail()
        //     .isString().withMessage('Please enter a valid review'),
        //     SingleBodyValidationMiddleware,
        //     body('rating').notEmpty().withMessage('Please enter a valid rating').bail()
        //     .isString().withMessage('Please enter a valid rating').bail()
        //     .toInt(),
        //     SingleBodyValidationMiddleware,
        //     postRatingReviewController
        // ]);
        // this.app.post('/uploadfile/:order_item_id', [
        //     accessTokenVerificationMiddleware,
        //     param('order_item_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
        //     .toInt(),
        //     SingleBodyValidationMiddleware,
        //     postImageController
        // ]);
        this.app.post('/review/:order_item_id', [
            accessTokenPrivateVerificationMiddleware,
            param('order_item_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid order-item-id').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            postRatingReviewController
        ]);
        return this.app;
    }
}