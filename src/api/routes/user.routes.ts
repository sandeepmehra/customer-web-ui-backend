import express from 'express';
import { body, param } from 'express-validator';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import { addressInfoPostController, addressInfoUpdateController, addToWishlistController, getUserAddressController, getUserInfoController, getWishlistController, removeToWishlistController, updateInfoController } from '../controllers/user.controller';
import {
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';


export class UserRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'UserRoutes');
    }
    configureRoutes(): express.Application {
        this.app.put('/account', [
            accessTokenPrivateVerificationMiddleware,
            body('email').optional().isEmail().withMessage('Please enter a valid Email'),
            SingleBodyValidationMiddleware,
            body('name').optional().notEmpty().withMessage('Please enter a valid Name').bail()
            .isString().withMessage('Please enter a valid Name'),
            SingleBodyValidationMiddleware,
            body('mobile_number').optional().notEmpty().withMessage('Please enter a valid Mobile Number').bail()
            .isString().withMessage('Please enter a valid Mobile Number').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid Mobile Number'),
            SingleBodyValidationMiddleware,
            updateInfoController
        ]);

        this.app.get('/account', [
            accessTokenPrivateVerificationMiddleware,
            getUserInfoController
        ]);

        this.app.get('/account/address', [
            accessTokenPrivateVerificationMiddleware,
            getUserAddressController
        ]);

        this.app.post('/account/address', [
            accessTokenPrivateVerificationMiddleware,
            body('name').notEmpty().withMessage('Please enter a valid Name').bail()
            .isString().withMessage('Please enter a valid Name'),
            SingleBodyValidationMiddleware,
            body('city').notEmpty().withMessage('Please enter a valid City').bail()
            .isString().withMessage('Please enter a valid City'),
            SingleBodyValidationMiddleware,
            body('street').notEmpty().withMessage('Please enter a valid Street').bail()
            .isString().withMessage('Please enter a valid Street'),
            SingleBodyValidationMiddleware,
            body('state').notEmpty().withMessage('Please enter a valid State').bail()
            .isString().withMessage('Please enter a valid State'),
            SingleBodyValidationMiddleware,
            body('pincode').notEmpty().withMessage('Please enter a valid pincode').bail()
            .isString().withMessage('Please enter a valid pincode').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('mobile_number').notEmpty().withMessage('Please enter a valid Mobile Number').bail()
            .isString().withMessage('Please enter a valid Mobile Number').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid Mobile Number'),
            SingleBodyValidationMiddleware,
            addressInfoPostController
        ]);

        this.app.put('/account/:customer_id/address', [
            accessTokenPrivateVerificationMiddleware,
            param('customer_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid user').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('name').optional().notEmpty().withMessage('Please enter a valid Name').bail()
            .isString().withMessage('Please enter a valid Name'),
            SingleBodyValidationMiddleware,
            body('city').optional().notEmpty().withMessage('Please enter a valid City').bail()
            .isString().withMessage('Please enter a valid City'),
            SingleBodyValidationMiddleware,
            body('street').optional().notEmpty().withMessage('Please enter a valid Street').bail()
            .isString().withMessage('Please enter a valid Street'),
            SingleBodyValidationMiddleware,
            body('state').optional().notEmpty().withMessage('Please enter a valid State').bail()
            .isString().withMessage('Please enter a valid State'),
            SingleBodyValidationMiddleware,
            body('pincode').optional().notEmpty().withMessage('Please enter a valid pincode').bail()
            .isString().withMessage('Please enter a valid pincode').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('mobile_number').optional().notEmpty().withMessage('Please enter a valid Mobile Number').bail()
            .isString().withMessage('Please enter a valid Mobile Number').bail()
            .isMobilePhone('en-IN').withMessage('Please enter a valid Mobile Number'),
            SingleBodyValidationMiddleware,
            addressInfoUpdateController
        ]);
        this.app.get('/wishlist', [
            accessTokenPrivateVerificationMiddleware,
            getWishlistController
        ]);
        this.app.post('/wishlist/:product_variant_id', [
            accessTokenPrivateVerificationMiddleware,
            param('product_variant_id').notEmpty().withMessage('Please enter a valid id').bail()
            .isString().withMessage('Please enter a valid id'),
            param('product_variant_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid user').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            addToWishlistController
        ]);
        this.app.delete('/wishlist/:wishlist_id', [
            accessTokenPrivateVerificationMiddleware,
            param('wishlist_id').notEmpty().withMessage('Please enter a valid id').bail()
            .isString().withMessage('Please enter a valid id'),
            param('wishlist_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid user').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            removeToWishlistController
        ]);
        return this.app;
    }
}