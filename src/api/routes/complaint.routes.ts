import express from 'express';
import { body, param } from 'express-validator';
import {SingleBodyValidationMiddleware} from "../middlewares/common.middleware";
import { RoutesConfig } from "../../config/routes.config";
import {
    accessTokenPrivateVerificationMiddleware
} from '../middlewares/auth.middleware';
import { getByIdComplaintController, getComplaintController, postComplaintController, postComplaintResponseController } from '../controllers/complaint.controller';


export class ComplaintRoutes extends RoutesConfig {
    constructor(app: express.Application) {
        super(app, 'ComplaintRoutes');
    }
    configureRoutes(): express.Application {
        this.app.get('/complaints', [
            accessTokenPrivateVerificationMiddleware,
            getComplaintController
        ])

        this.app.get('/complaint/:complaint_id', [
            accessTokenPrivateVerificationMiddleware,
            param('complaint_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid user').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            getByIdComplaintController
        ]);

        this.app.post('/complaint', [
            accessTokenPrivateVerificationMiddleware,
            SingleBodyValidationMiddleware,
            body('subject').notEmpty().withMessage('Please enter a valid subject').bail()
            .isString().withMessage('Please enter a valid subject'),
            SingleBodyValidationMiddleware,
            body('description').notEmpty().withMessage('Please enter a valid description').bail()
            .isString().withMessage('Please enter a valid description'),
            SingleBodyValidationMiddleware,
            postComplaintController
        ]);
        this.app.post('/response/:complaint_id', [
            accessTokenPrivateVerificationMiddleware,
            param('complaint_id').custom((value) => ((typeof value === "string") && !isNaN(Number(value)) && value !== "")).withMessage('Please enter a valid user').bail()
            .toInt(),
            SingleBodyValidationMiddleware,
            body('response').notEmpty().withMessage('Please enter a valid response').bail()
            .isString().withMessage('Please enter a valid response'),
            SingleBodyValidationMiddleware,
            postComplaintResponseController
        ]);
        return this.app;
    }
}