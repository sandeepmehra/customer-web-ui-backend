import prisma from "../../../prisma/prisma-client";
import {
    combineArrayDto,
    complaintsDto,
    complaintsResponseDto,
    complaintsStatusDBDto,
    complaintsStatusResponseDto,
    responsesArrayDto
} from "../dtos/complaint.dto";
import {
    convertBigInt2String
} from "../helpers/convert.helper";
require('dotenv').config();

export const getComplaintService = async (email ? : string, mobile_number ? : string) => {
    try {
        let complaintsResult:any;
        if (email !== undefined) {
            complaintsResult= await prisma.user_customer.findUnique({
                where: {
                    email: email
                },
                select: {
                    "complaints": {
                        orderBy: [{
                            'created_on': 'desc'
                        }],
                        select: {
                            "id": true,
                            "subject": true,
                            "open": true,
                            "created_on": true,
                            "closed_on": true
                        }
                    }
                }
            })
        } else {
            complaintsResult = await prisma.user_customer.findUnique({
                where: {
                    mobile_number: mobile_number 
                },
                select: {
                    "complaints": {
                        orderBy: [{
                            'created_on': 'desc'
                        }],
                        select: {
                            "id": true,
                            "subject": true,
                            "open": true,
                            "created_on": true,
                            "closed_on": true
                        }
                    }
                }

            });
        }
        return {
            status: 200,
            complaints: post_process_complaints(convertBigInt2String(complaintsResult?.complaints))
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const getByIdComplaintService = async (id: number, email ? : string, mobile_number ? : string) => {
    try {
        let complaintData: any;
        if (email !== undefined) {
            complaintData = await prisma.complaint.findMany({
                where: {
                    AND: [{
                        id: Number(id)
                    }, {
                        customer: {
                            email: email
                        }
                    }]
                },
                select: {
                    'id': true,
                    'subject': true,
                    'description': true,
                    'open': true,
                    'created_on': true,
                    'closed_on': true,
                    customer_response: {
                        select: {
                            'id': true,
                            'response': true,
                            'created_on': true,
                            'user': {
                                select: {
                                    'name': true,
                                }
                            }
                        }
                    },
                    admin_response: {
                        select: {
                            'id': true,
                            'response': true,
                            'created_on': true,
                            'user': {
                                select: {
                                    'name': true,
                                }
                            }
                        }
                    }
                }
            });
        } else {
            complaintData = await prisma.complaint.findMany({
                where: {
                    AND: [{
                        id: Number(id)
                    }, {
                        customer: {
                            mobile_number: mobile_number
                        }
                    }]
                },
                select: {
                    'id': true,
                    'subject': true,
                    'description': true,
                    'open': true,
                    'created_on': true,
                    'closed_on': true,
                    customer_response: {
                        select: {
                            'id': true,
                            'response': true,
                            'created_on': true,
                            'user': {
                                select: {
                                    'name': true,
                                }
                            }
                        }
                    },
                    admin_response: {
                        select: {
                            'id': true,
                            'response': true,
                            'created_on': true,
                            'user': {
                                select: {
                                    'name': true,
                                }
                            }
                        }
                    }
                }
            });
        }
        return {
            "status": 200,
            "complaint": post_process_complaint(convertBigInt2String(complaintData))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const postComplaintService = async (complaintDataDto: {
    subject: string;description: string
}, email ? : string, mobile_number?:string) => {
    try {
        if (email !== undefined) {
            await prisma.complaint.create({
                data: {
                    "subject": complaintDataDto.subject,
                    "description": complaintDataDto.description,
                    "customer": {
                        connect: {
                            email: email
                        }
                    }
                }
            })
        }else {
            await prisma.complaint.create({
                data: {
                    "subject": complaintDataDto.subject,
                    "description": complaintDataDto.description,
                    "customer": {
                        connect: {
                            mobile_number: mobile_number
                        }
                    }
                }
            })
        }
        return {
            "status": 201,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const postComplaintResponseService = async (complaint_id: number, responseDto:{response: string}, email ? : string, mobile_number ? : string) => {
    try {
        if (email !== undefined) {
            await prisma.customer_response.create({
                data: {
                    "response": responseDto.response,
                    "user":{connect: { email: email}},
                    "complaint":{connect: { id: complaint_id}}
                }
            });
        }else{
            await prisma.customer_response.create({
                data: {
                    "response": responseDto.response,
                    "user":{connect: { mobile_number: mobile_number}},
                    "complaint":{connect: { id: complaint_id}}
                }
            });
        }
        return {
            "status": 201,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


const post_process_complaints = (complaintData: any) => {
    let complaintData_result: complaintsDto = < complaintsDto > {};
    complaintData_result['in_progress'] = check_status_true(complaintData);
    complaintData_result['resolved'] = check_status_false(complaintData);
    return complaintData_result;
};

const check_status_true = (statusData: any) => {
    let resposnes_result: Array < complaintsStatusResponseDto > = []
    if (statusData.length > 0) {
        statusData.forEach((element: complaintsStatusDBDto) => {
            if (element.open === true) {
                resposnes_result.push({
                    'id': element.id,
                    'subject': element.subject,
                    'created_on': element.created_on
                });
            }
        });
        return resposnes_result;
    }
}

const check_status_false = (statusData: any) => {
    let resposnes_result: Array < complaintsStatusResponseDto > = []
    if (statusData.length > 0) {
        statusData.forEach((element: complaintsStatusDBDto) => {
            if (element.open === false) {
                resposnes_result.push({
                    'id': element.id,
                    'subject': element.subject,
                    'closed_on': element.closed_on
                });
            }
        });
        return resposnes_result;
    }
}



const post_process_complaint = (complaintData: any) => {
    let complaintData_result: complaintsResponseDto = < complaintsResponseDto > {};
    if (complaintData.length > 0) {
        complaintData_result['id'] = complaintData[0]['id'];
        complaintData_result['subject'] = complaintData[0]['subject'];
        complaintData_result['description'] = complaintData[0]['description'];
        complaintData_result['open'] = complaintData[0]['open'];
        complaintData_result['created_on'] = complaintData[0]['created_on'];
        complaintData_result['closed_on'] = complaintData[0]['open'] !== false ? complaintData[0]['closed_on'] : undefined;
        complaintData_result['responses'] = post_process_responses(complaintData[0]['customer_response'], complaintData[0]['admin_response']);

        return complaintData_result;
    }
    return {}
}

const post_process_responses = (customerResponses: any, adminResponse: any) => {
    let combineArray: Array < combineArrayDto > = [];

    adminResponse.forEach((element: any) => element['user']['name'] = element['user']['name'] + " [Urban Plants]");

    const combinedResponses = combineArray.concat(customerResponses, adminResponse).sort((a: combineArrayDto, b: combineArrayDto) => (a.created_on < b.created_on) ? 1 : ((b.created_on < a.created_on) ? -1 : 0));
    let resposnes_result: Array < responsesArrayDto > = []
    if (combinedResponses.length > 0) {
        combinedResponses.forEach((element: any) => {
            resposnes_result.push({
                'from': element.user.name,
                'datetime': element.created_on,
                'response': element.response
            })
        });
        return resposnes_result;
    }
}