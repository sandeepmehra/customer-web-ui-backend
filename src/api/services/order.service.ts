import prisma from "../../../prisma/prisma-client";
import { orderAddressResponseDto, orderDBDto, orderResponseDto, ordersResponseDto } from "../dtos/order.dto";
import {
    convertBigInt2String
} from "../helpers/convert.helper";
import { res_order_status } from "../helpers/status.helper";
import { pdfTemplate } from '../documents';
import fs from "fs";
import { createClient } from '@supabase/supabase-js';
import logger from "../../config/logger.config";
import https from 'https';
import moment from "moment";
require('dotenv').config();

const supabase = createClient(process.env.SUPABASE_STORAGE_BASE_URL as string, process.env.SUPABASE_STORAGE_BASE_ANON_KEY as string);

const PaytmChecksum = require('paytmchecksum');
const puppeteer = require('puppeteer');
// let chrome:any = {};
// let puppeteer:any;

// if (process.env.AWS_LAMBDA_FUNCTION_VERSION) {
//   chrome = require("chrome-aws-lambda");
//   puppeteer = require("puppeteer-core");
// } else {
//   puppeteer = require("puppeteer");
// }

let paytmMerchantkey = process.env.PAYTM_MERCHANT_KEY;

export const getOrdersService = async (email ? : string, mobile_number ? : string) => {
    console.log("getOrdersService");
    try {
        let ordersResult:any;
        if (email !== undefined) {
            ordersResult= await prisma.orders.findMany({
                where: {
                    AND: [{
                        payment_txn_status: 'TXN_SUCCESS'
                    }, {
                        customer: {
                            email: email
                        }
                    }]
                },
                orderBy: [{
                    'placed_on': 'desc'
                }],
                select: {
                   order_items: {
                        select: {
                            id: true,
                            quantity: true,
                            price_per_unit: true,
                            status: true,
                            product_variant: {
                                select: {
                                    product_variant_images:{
                                        select: {
                                            url: true
                                        }
                                    },
                                    product: {
                                        select: {
                                            name: true,
                                            shop:{
                                                select:{ 
                                                    name: true
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                       }
                   }
                }
            });
        } else {
            ordersResult= await prisma.orders.findMany({
                where: {
                    customer:{mobile_number: mobile_number}
                },
                orderBy: [{
                    'placed_on': 'desc'
                }],
                select: {
                    id:true,
                    order_items: {
                        select: {
                            id: true,
                            quantity: true,
                            price_per_unit: true,
                            status: true,
                            product_variant: {
                            select: {
                                product_variant_images:{
                                    select: {
                                        url: true
                                    }
                                },
                                product: {
                                    select: {
                                        name: true,
                                        shop:{
                                            select:{ 
                                                name: true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                       }
                   }
                }
            });
        }
        return {
            status: 200,
            orders: post_process_orders(convertBigInt2String(ordersResult))
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const getOrderDetailService = async (id: number, email ? : string, mobile_number ? : string) => {
    try {
        let order: any;
        if (email !== undefined) {
            order = await prisma.order_item.findMany({
                where: {
                    AND: [{
                        id: Number(id)
                    }, {
                        orders:{
                            customer: {
                                email: email
                            }
                        }
                    }]
                },
                select: {
                    id: true,
                    quantity: true,
                    price_per_unit: true,
                    status: true,
                    orders: {
                        select: {
                            id: true,
                            customer_order_address:{
                                select: {
                                    name: true,
                                    mobile_number: true,
                                    address: {
                                        select:{
                                            city: true,
                                            state: true,
                                            street: true,
                                            pincode: true
                                        }
                                    }
                                }
                            }
                        }
                    },
                    product_variant: {
                        select: {
                            product_variant_images:{
                            select: {
                                url: true
                                }
                            },
                            product: {
                                select: {
                                    name: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    },
                    order_item_review:{
                        select: {
                            action_taken: true
                        }
                    }
                }
            });
        } else {
            order = await prisma.order_item.findMany({
                where: {
                    AND: [{
                        id: Number(id)
                    }, {
                        orders:{
                            customer: {
                                mobile_number: mobile_number
                            }
                        }
                    }]
                },
                select: {
                    id: true,
                    quantity: true,
                    price_per_unit: true,
                    status: true,
                    orders: {
                        select: {
                            id: true,
                            customer_order_address:{
                                select: {
                                    name: true,
                                    mobile_number: true,
                                    address: {
                                        select:{
                                            city: true,
                                            state: true,
                                            street: true,
                                            pincode: true
                                        }
                                    }
                                }
                            }
                        }
                    },
                    product_variant: {
                        select: {
                            product_variant_images:{
                            select: {
                                url: true
                                }
                            },
                            product: {
                                select: {
                                    name: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    },
                    order_item_review:{
                        select: {
                            action_taken: true
                        }
                    }
                }
            });
        }
        return {
            "status": 200,
            "order": post_process_order(convertBigInt2String(order))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const placeOrderService = async ( body: any, email ? : string, mobile_number ? : string) => {
    try {
        if (email !== undefined) {
            const customerId:any = await prisma.user_customer.findUnique({
                where:{email: email},
                select: {
                    id: true
                }
            });
            let createdOrder:any;
            let total_price:any;
            if(Object.keys(body.product_detail).length !== 0){
                const productVariantData: any = await prisma.product_variant.findUnique({
                    where:{id: Number(body.product_detail.productVariantId)},
                    select: {
                        "price": true,
                        "product": {
                            select: {
                                shop:{
                                    select:{
                                        seller: {
                                            select: {
                                                id: true,
                                                commission_percentage: true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                total_price = Number(post_process_multiply( Number(productVariantData.price),Number(body.product_detail.quantity)));
                createdOrder = await prisma.orders.create({
                    data:{
                        total_price: total_price,
                        customer_id: Number(customerId.id),
                        customer_order_address_id: Number(body.shipping_address_id),
                        order_items:{
                            createMany:{
                                data: {
                                    "quantity": body.product_detail.quantity,
                                    "seller_id": Number(productVariantData.product.shop.seller.id),
                                    "product_variant_id": Number(body.product_detail.productVariantId),
                                    "seller_commission": productVariantData.product.shop.seller.commission_percentage,
                                    "price_per_unit": productVariantData.price,
                                    "status": 'Shipped',
                                    "dispatch_date": new Date()
                                }
                            }
                        }
                    }
                });
            }else{
                const customerCartOrder = await prisma.cart_item.findMany({
                    where:{
                        customer_id: Number(customerId.id)
                    },
                    select:{
                        quantity: true,
                        seller_id: true,
                        seller:{
                            select:{
                                commission_percentage: true
                            }
                        },
                        product_variant_id: true,
                        product_variant: {
                            select: {
                                price: true
                            }
                        }
                    }
                });
                let orderItemData : Array<any> = [];
                total_price = count_total_amount(customerCartOrder);
                const orderItemDataCart = post_process_orderItem(customerCartOrder);
                if(orderItemDataCart.length === 0 && orderItemDataCart === undefined){
                    return {
                        "status": 400,
                        "message": 'Cart is Empty'
                    }
                }
                orderItemDataCart.forEach((item: any) => {
                    orderItemData.push(item);
                });
                createdOrder = await prisma.orders.create({
                    data:{
                        total_price: Number(total_price),
                        customer_id: Number(customerId.id),
                        customer_order_address_id: Number(body.shipping_address_id),
                        order_items:{
                            createMany:{
                                data: orderItemData
                            }
                        }
                    }
                });
            }
            let paytmParams: any = {
                "ORDER_ID": String(createdOrder.id),
                "CUST_ID": String(customerId.id),
                "INDUSTRY_TYPE_ID": process.env.PAYTM_INDUSTRY_TYPE_ID,
                "CHANNEL_ID": process.env.PAYTM_CHANNEL_ID,
                "TXN_AMOUNT": String(Number(Math.round(total_price))),
                "MID": process.env.PAYTM_MERCHANT_ID,
                "WEBSITE": process.env.PAYTM_WEBSITE,
                "MOBILE_NO": '7536885806',
                "EMAIL": email,
                "CALLBACK_URL": process.env.PAYTM_CALLBACK_URL,
            }
            let paytmCheckSum = await PaytmChecksum.generateSignature(paytmParams, paytmMerchantkey);
            let params = {
                ...paytmParams,
                'CHECKSUMHASH': paytmCheckSum
            };
            await prisma.cart_item.deleteMany({where: {customer_id: Number(customerId.id)},});
            return {
                status: 200,
                params: params
            }
        }           
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const paymentServiceCallback = async (req: any, res: any) => {
    try {
        var paytmCheckSum = "";
        var received_data = req.body;
        console.log("received_data", received_data);
        if(received_data === undefined || received_data === null || received_data === ''){
            logger('error', 'POST', `/place-order/callback`, 400, 'received_data is not an object');
            return res.status(400).json({
                "message": 'Something went wrong in payment'
            });
        }
        var paytmParams: any = {};
        for (var key in received_data) {
            if (key == "CHECKSUMHASH") {
                paytmCheckSum = received_data[key];
            } else {
                paytmParams[key] = received_data[key];
            }
        }
        var isVerifySignature = PaytmChecksum.verifySignature(paytmParams, paytmMerchantkey, paytmCheckSum);
        if (isVerifySignature) {
            var paytmParams: any = {};
            paytmParams["MID"] = received_data['MID'];
            paytmParams["ORDERID"] = received_data['ORDERID'];

            PaytmChecksum.generateSignature(paytmParams, paytmMerchantkey).then(function (checksum: any) {
                paytmParams["CHECKSUMHASH"] = checksum;
                var post_data = JSON.stringify(paytmParams);
                console.log("post_data: ", post_data);
                var options = {
                    hostname: 'securegw-stage.paytm.in',
                    port: 443,
                    path: '/order/status',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Length': post_data.length
                    }
                };

                var res_result: any = '';
                var post_req = https.request(options, function (post_res:any) {
                    post_res.on('data', function (chunk:any) {
                        res_result += chunk;
                    });
                    post_res.on('end', async function() {
                        let result = JSON.parse(res_result);
                        if (result['STATUS'] === 'TXN_SUCCESS') {                   //store in db
                            await prisma.orders.update({
                                where:{
                                    id: Number(result['ORDERID'])
                                },
                                data:{
                                    payment_txn_id: result['TXNID'],
                                    payment_bank_txn_id: result['BANKTXNID'], 
                                    payment_txn_type: result['TXNTYPE'], 
                                    payment_gateway_name: result['GATEWAYNAME'], 
                                    payment_respcode: result['RESPCODE'], 
                                    payment_bankname: result['BANKNAME'], 
                                    payment_mid: result['MID'], 
                                    payment_mode: result['PAYMENTMODE'], 
                                    payment_refund_amt: result['REFUNDAMT'], 
                                    payment_txn_date: result['TXNDATE'], 
                                    payment_txn_status: 'TXN_SUCCESS'
                                }
                            });
                            const orderItemsData = await prisma.orders.findUnique({
                                where:{
                                    id: Number(result['ORDERID'])
                                },
                                select:{
                                    order_items:{
                                        select:{
                                            id: true,
                                            price_per_unit: true,
                                            seller_commission: true,
                                            quantity: true,
                                            product_variant:{
                                                select:{
                                                    product:{
                                                        select:{
                                                            description: true
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    customer:{
                                        select:{
                                            name: true,
                                            email: true,
                                            mobile_number: true
                                        }
                                    },
                                    customer_order_address:{
                                        select:{
                                            name: true,
                                            mobile_number: true,
                                            address:{
                                                select:{
                                                    city:true,
                                                    street: true,
                                                    state: true,
                                                    pincode: true
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                            if(orderItemsData === null) {
                                logger('error', 'GET', `/place-order/callback`, 400, 'Order Item is empty.');
                                return res.status(400).json({message: 'Order Item is empty.'});
                            }
                            let resRawData:any;
                            orderItemsData.order_items.forEach(async(item:any)=>{
                                console.log("order_items item id", item.id)
                                const invoiceData = await prisma.invoice_order_item.create({
                                    data: {
                                        order_id: Number(item.id)
                                    }
                                });
                                console.log("invoiceData", invoiceData)
                                const template_data:any = {
                                    orderItemId: Number(item.id),
                                    invoiceNumber: moment(invoiceData.added_on).format('YYYYMMDD')+invoiceData.invoice_number,
                                    invoiceDate: String(moment(invoiceData.added_on).format('DD/MM/YYYY')),
                                    name: orderItemsData.customer.name || orderItemsData.customer_order_address.name,
                                    mobileNumber: orderItemsData.customer_order_address.mobile_number || orderItemsData.customer.mobile_number,
                                    street: orderItemsData.customer_order_address.address.street,
                                    city: orderItemsData.customer_order_address.address.city,
                                    state: orderItemsData.customer_order_address.address.state,
                                    pincode: orderItemsData.customer_order_address.address.pincode,
                                    txnId: result['TXNID'],
                                    paymentMode: 'Paytm',
                                    paymentDateTime: result['TXNDATE'],
                                    productDescription: item.product_variant.product.description,
                                    productUnitPrice: item.price_per_unit,
                                    productQty: item.quantity
                                };
                                console.log("template_data", template_data)
                                let errorArr:any = [];
                                let options = {};
                                console.log("process.env.AWS_LAMBDA_FUNCTION_VERSION: ", process.env.AWS_LAMBDA_FUNCTION_VERSION);
                                // if (process.env.AWS_LAMBDA_FUNCTION_VERSION) {
                                //     options = {
                                //     args: [...chrome.args, "--hide-scrollbars", "--disable-web-security"],
                                //     defaultViewport: chrome.defaultViewport,
                                //     executablePath: await chrome.executablePath,
                                //     headless: true,
                                //     ignoreHTTPSErrors: true,
                                //     };
                                // }
                                if (process.env.AWS_LAMBDA_FUNCTION_VERSION) {
                                    let browser = await puppeteer.launch(options);
                                    const page = await browser.newPage();
                                    const content = pdfTemplate(template_data);
                                    await page.setContent(content,  { waitUntil: 'networkidle0' });
                                    resRawData = await page.pdf({
                                        format: 'A4',
                                        displayHeaderFooter: false,
                                        margin: {
                                            top: '30px',
                                            bottom: '30px',
                                            left: '30px',
                                            right: '30px'
                                        },
                                        printBackground: true,
                                    });
                                    console.log("resRawData", 'resRawData')
                                    const {data, error}:any = await supabase.storage
                                        .from(process.env.DOCUMENT_BUCKET_NAME as string)
                                        .upload('invoices/'+ `${String(item.id)}-order-invoice.pdf`, resRawData, {contentType: 'application/pdf'});

                                    if(data === undefined){
                                        logger('error', 'POST', `/place-order/callback`, 400, 'Promise is not resolved');
                                        errorArr.push({msg: 'Promise is not resolved'});
                                    }
                                    if(error){
                                        logger('error', 'POST', `/place-order/callback`, 400, error.value.error.message);
                                        errorArr.push({msg: 'Duplicate file in supabase storage bucket'});
                                    }  
                                    await prisma.invoice_order_item.update({
                                        where:{
                                            order_id: item.id,
                                        },
                                        data: {
                                            invoice_url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+data.Key
                                        }
                                    }); 
                                    await browser.close();  
                                }
                                    
                                });   
                            return res.redirect(`${process.env.UI_URL}/order-success`);
                        } else { //Status TXN_FAILURE
                            const orderData = await prisma.orders.findUnique({
                                where:{
                                    id: Number(result['ORDERID'])
                                }, 
                                select:{
                                    order_items:{
                                        select:{
                                            quantity: true,
                                            seller_id: true,
                                            product_variant_id:true,
                                        }
                                    },
                                    customer_id: true
                                }
                            });
                            
                            if(orderData !== undefined && orderData !== null && orderData.order_items !== undefined && orderData.customer_id !== undefined){
                                const cartItemData = post_process_cartItemData(orderData.order_items, orderData.customer_id);
                                await prisma.cart_item.createMany({data: cartItemData});
                            }
                            await prisma.order_item.deleteMany({where: {order_id: Number(result['ORDERID'])},});
                            await prisma.orders.delete({where: {id: Number(result['ORDERID']),},});
                            return res.redirect(`${process.env.UI_URL}/order-failure`);
                        }
                    });
                });
                post_req.write(post_data);
                post_req.end();
            });
        } else {
            logger('error', 'POST', `/place-order/callback`, 500, 'Checksum Mismatched! STOP MESSING AROUND WITH GATEWAY.');
            return res.status(400).json({
                "message": "STOP MESSING AROUND WITH GATEWAY"
            });
        }
    } catch (error:any) {
        logger('error', 'POST', `/place-order/callback`, 500, error.message);
        return res.status(500).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
}

export const getOrderInvoiceService = async (id: number, req:any, res:any, email ? : string, mobile_number ? : string) => {
    try {
        if(email !== undefined){
            let invoice: any;
            invoice = await prisma.invoice_order_item.findUnique({
                where: {
                    order_id: Number(id)
                },
                select: {
                    invoice_number: true,
                    invoice_url: true,
                    added_on: true,
                },
            });
            console.log("invoice:", invoice);
            if(invoice === null || invoice.invoice_url === null || invoice.invoice_url === undefined ){
                logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, 'Invoice url is null');
                return res.status(400).json({message: 'Invoice url is null'})
            }
            const fileName = `${id}-order-invoice.pdf`;
            const filePath = 'invoices/' + fileName;
            const {data, error}:any = await supabase.storage
                        .from(process.env.DOCUMENT_BUCKET_NAME as string)
                        .download(filePath);
            if(error){
                logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, error);
                return res.status(400).json({message: 'Duplicate file in supabase storage bucket'});
            } 
            const buffer = Buffer.from( await data.arrayBuffer() );
            await fs.promises.writeFile(`${__dirname}/${fileName}`, buffer);
            res.setHeader('Content-Length', data.size);
            res.setHeader('Content-Type', 'application/pdf');
            return res.status(200).download(`${__dirname}/${fileName}`, fileName, function(error:any) {
                if (error) {
                    logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, error);
                    return res.status(400).json({message: 'Something in wrong when file in download'}); 
                }
                fs.unlinkSync(`${__dirname}/${fileName}`);
            });
        }else{
            let invoice: any;
            invoice = await prisma.invoice_order_item.findUnique({
                where: {
                    order_id: Number(id)
                },
                select: {
                    invoice_number: true,
                    invoice_url: true,
                    added_on: true,
                },
            });
            if(invoice === null || invoice.invoice_url === null || invoice.invoice_url === undefined ){
                logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, 'Invoice url is null');
                return res.status(400).json({message: 'Invoice url is null'})
            }
            const fileName = `${id}-order-invoice.pdf`;
            const filePath = 'invoices/' + fileName;
            const {data, error}:any = await supabase.storage
                        .from(process.env.DOCUMENT_BUCKET_NAME as string)
                        .download(filePath);
            if(error){
                logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, error);
                return res.status(400).json({message: 'Duplicate file in supabase storage bucket'});
            } 
            const buffer = Buffer.from( await data.arrayBuffer() );
            await fs.promises.writeFile(`${__dirname}/${fileName}`, buffer);
            res.setHeader('Content-Length', data.size);
            res.setHeader('Content-Type', 'application/pdf');
            return res.status(200).download(`${__dirname}/${fileName}`, fileName, function(error:any) {
                if (error) {
                    logger('error', 'GET', `/order/${id}/payment/invoice/download`, 400, error);
                    return res.status(400).json({message: 'Something in wrong when file in download'}); 
                }
                fs.unlinkSync(`${__dirname}/${fileName}`);
            });
        }
    } catch (error: any) {
        logger('error', 'GET', `/order/${id}/payment/invoice/download`, 500, error.message);
        return res.status(500).json({
            "message": process.env.RESPONSE_INTERNAL_SERVER_ERROR
        });
    }
}


const post_process_cartItemData = (orderItem:any, customer_id:any) => {
    let cartItem_result: Array<any> = [];
    if (orderItem.length > 0) {
        orderItem.forEach((element: any) => {
            cartItem_result.push({
                "quantity": element.quantity,
                "seller_id": element.seller_id,
                "product_variant_id": element.product_variant_id,
                "customer_id": customer_id
            });
        });
        return cartItem_result;
    }
    return []
}
const post_process_orderItem = (orderItem:any) => {
    let orderItem_result: Array<any> = [];
    if (orderItem.length > 0) {
        orderItem.forEach((element: any) => {
            orderItem_result.push({
                "quantity": element.quantity,
                "seller_id": element.seller_id,
                "product_variant_id": element.product_variant_id,
                "seller_commission": element.seller.commission_percentage,
                "price_per_unit": element.product_variant.price,
                "status": 'Shipped',
                "dispatch_date": new Date()
            });
        });
        return orderItem_result;
    }
    return []
}

const count_total_amount = (orderItem:any) => {
    let orderItem_result: Array<any> = [];
    let total:number = 0;
    if (orderItem.length > 0) {
        orderItem.forEach((element: any) => {
            orderItem_result.push(Number(post_process_multiply(element.quantity, element.product_variant.price)));
        });
        orderItem_result.forEach((item:any) => {
            total += parseFloat(item)
        }); 
        return total
    }
}

const post_process_order = (orderData: any) => {
    let order_result: orderResponseDto = < orderResponseDto > {};
    if (orderData.length > 0) {
        order_result['id'] = orderData[0]['id'];
        order_result['order_id'] = String(orderData[0].orders.id);
        order_result['product_name'] = orderData[0].product_variant.product.name;
        order_result['total_price'] = post_process_multiply(orderData[0].quantity, orderData[0].price_per_unit),
        order_result['quantity'] = String(orderData[0].quantity);
        order_result['status_text'] = res_order_status(orderData[0].status);
        order_result['shop_name'] = orderData[0].product_variant.product.shop.name;
        order_result['image'] = orderData[0].product_variant.product_variant_images.length > 0 ? orderData[0].product_variant.product_variant_images[0].url : '';
        order_result['address'] = address(orderData[0].orders.customer_order_address); 
        order_result['review_action_taken'] = orderData[0]?.order_item_review?.action_taken ? orderData[0].order_item_review.action_taken : false;

        return order_result;
    }
    return {}
}

const address = (address: any) => {
    let order_address: orderAddressResponseDto = < orderAddressResponseDto > {};
    order_address['name'] = address['name'];
    order_address['mobile_number'] = address['mobile_number'];
    order_address['city'] = address.address['city'];
    order_address['state'] = address.address['state'];
    order_address['street'] = address.address['street'];
    order_address['pincode'] = String(address.address['pincode']);

    return order_address;
}

const post_process_orders = (ordersRes: any) => {
    let orderArr: Array < any > = [];
    if (ordersRes.length > 0) {
        ordersRes.forEach((element: any) => {
            orderArr.push(orders_item_result(element.order_items));
        });
        return orderArr.flat();
    }
}

const orders_item_result = (orderRes: any) => {
    let orderArr: Array < ordersResponseDto > = [];
    if (orderRes.length > 0) {
        orderRes.forEach((element: orderDBDto) => {
            orderArr.push({
                "id": String(element.id),
                "quantity": String(element.quantity),
                "status_text": res_order_status(element.status),
                "total_price": post_process_multiply(element.quantity, element.price_per_unit),
                "product_name": element.product_variant.product.name,
                "shop_name": element.product_variant.product.shop.name,
                "image": element.product_variant.product_variant_images.length > 0 ? element.product_variant.product_variant_images[0].url : '',
            });
        });
        return orderArr;
    }
}

const post_process_multiply = (a: number, b: number) => {
    return String(Math.round((a*b) * 100) / 100);
}
