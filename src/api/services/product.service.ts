import prisma from "../../../prisma/prisma-client";
import { productInfoDto, productResponseDto, productVariantResponseDto } from "../dtos/product.dto";
import { convertBigInt2String } from "../helpers/convert.helper";
require('dotenv').config();


export const productService = async (id: number) => {
    try {
        const product = await prisma.product.findUnique({
            where: {
                "id": Number(id)
            },
            select: {
                "name": true,
                "description": true,
                "product_category": true,
                "shop": {
                    select:{
                        "name": true
                    }
                },
                "product_variants": {
                    select: {
                        "id": true,
                        "color": true,
                        "measurement": true, 
                        "measurement_unit": true,
                        "price": true,
                        "discounted_price": true,
                        "sku": true,
                        "country_of_origin": true,
                        "product_variant_images": {
                            select: {
                                "url": true
                            }
                        },
                        "product_variant_videos": {
                            select: {
                                "url": true
                            }
                        }
                    }
                }
            }
        });
        
        return  {
                    "status": 200,
                    "product": post_process_product(product)
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const getAllCategoryService = async () => {
    try {
        const product_category_list = await prisma.categories.findMany({});
        return  {
                    "status": 200,
                    "product_category_list": post_process_product_category(product_category_list)
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const categoryService = async (category: string, email ? : string, mobile_number ? : string) => {
    try {
        let product_category_detail;
        let category_detail;
        let customerId:any;
        let bannerUrl:any;
        bannerUrl = await prisma.categories.findUnique({
            where:{
                category_name: category
            },
            select:{
                category_image_url:true
            }
        });
        if(email !== undefined && email !== null && email !== ''){
            customerId = await prisma.user_customer.findUnique({
                where:{email: email},
                select: {
                    id: true
                }
            });
            category_detail = await prisma.product_variant.findMany({
                where: {product:{"product_category": category}},
                select: {
                    "id": true
                }
            });
            const variant_category_ids = post_process_list_detail_ids(category_detail);
            let wishlistExists;
            if(variant_category_ids !== undefined && variant_category_ids !== ""){
                wishlistExists = await prisma.wishlist.findMany({
                    where:{AND:[{product_variant_id: { in: variant_category_ids }},{customer:{id: Number(customerId.id)}}]}
                });
                console.log("wishlistExists", wishlistExists);
            }
            
            product_category_detail = await prisma.product_variant.findMany({
                where: {
                    product:{"product_category": category}
                },
                select: {
                    "id": true,
                    "price": true,
                    "discounted_price": true,
                    "product":{
                        select:{
                            "id": true,
                            "name": true,
                            "description": true,
                            "product_category": true,
                            "shop":{
                                select: {
                                    name: true
                                }
                            },
                        },
                    },
                    "product_variant_images": {
                        select:{
                            url: true
                        }
                    }
                }
            });
            return  {
                "status": 200,
                "product_category_detail":{
                    "banner_url": bannerUrl.category_image_url,
                    "detail_list": list_detail_with_wishlist(product_category_detail, wishlistExists)
                }
            };
        }else if(mobile_number !== undefined && mobile_number !== null && email !== ''){
            customerId = await prisma.user_customer.findUnique({
                where:{mobile_number: mobile_number},
                select: {
                    id: true
                }
            });
            category_detail = await prisma.product_variant.findMany({
                where: {product:{"product_category": category}},
                select: {
                    "id": true
                }
            });
            const variant_category_ids = post_process_list_detail_ids(category_detail);
            let wishlistExists;
            if(variant_category_ids !== undefined && variant_category_ids !== ""){
                wishlistExists = await prisma.wishlist.findMany({
                    where:{AND:[{product_variant_id: { in: variant_category_ids }},{customer:{id: Number(customerId.id)}}]}
                });
            }
        
            product_category_detail = await prisma.product_variant.findMany({
                where: {
                    product:{"product_category": category}
                },
                select: {
                    "id": true,
                    "price": true,
                    "discounted_price": true,
                    "product":{
                        select:{
                            "id": true,
                            "name": true,
                            "description": true,
                            "product_category": true,
                            "shop":{
                                select: {
                                    name: true
                                }
                            },
                        },
                    },
                    "product_variant_images": {
                        select:{
                            url: true
                        }
                    }
                }
            });
            return  {
                "status": 200,
                "product_category_detail": {
                    "banner_url": bannerUrl.category_image_url,
                    "detail_list": list_detail_with_wishlist(product_category_detail, wishlistExists)
                }
            };
        }else{
            product_category_detail = await prisma.product_variant.findMany({
                where: {
                    product:{"product_category": category}
                },
                select: {
                    "id": true,
                    "price": true,
                    "discounted_price": true,
                    "product":{
                        select:{
                            "id": true,
                            "name": true,
                            "description": true,
                            "product_category": true,
                            "shop":{
                                select: {
                                    name: true
                                }
                            },
                        },
                    },
                    "product_variant_images": {
                        select:{
                            url: true
                        }
                    }
                }
            });
            return  {
                "status": 200,
                "product_category_detail": {
                    "banner_url": bannerUrl.category_image_url,
                    "detail_list": post_process_list_detail(product_category_detail)
                }
            };
        }        
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const categoryAlsoLikeService = async (category: string) => {
    try {
        let bannerUrl:any = await prisma.categories.findUnique({
            where:{
                category_name: category
            },
            select:{
                category_image_url:true
            }
        });
        const product_category_detail = await prisma.product.findMany({
            where: {
                "product_category": category
            },
            select: {
                "id": true,
                "name": true,
                "description": true,
                "product_category": true,
                "shop":{
                    select: {
                        name: true
                    }
                },
                "product_variants":{
                    select:{
                        "id": true,
                        "price": true,
                        "discounted_price": true,
                        "product_variant_images": {
                            select:{
                                url: true
                            }
                        },
                        "wishlist": {
                            select:{
                                id: true
                            },
                        },
                    },
                }, 
            },
        });
        // console.log("product_category_detail Alsolike: ",product_category_detail)
        return  {
                    "status": 200,
                    "product_category_detail": {
                        "banner_url": bannerUrl.category_image_url,
                        "detail_list": post_process_product_list_detail(product_category_detail)
                    }
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const specificCategoryService = async (category: string, limit: number) => {
    try {
        const product_category_detail = await prisma.product_variant.findMany({
            where: {
                product:{"product_category": category}
            },
            take: limit,
            orderBy: [{
                'updated_on': 'desc'
            }],
            select: {
                "id": true,
                "price": true,
                "discounted_price": true,
                "product":{
                    select:{
                        "id": true,
                        "name": true,
                        "description": true,
                        "product_category": true,
                        "shop":{
                            select: {
                                name: true
                            }
                        },
                    },
                },
                "product_variant_images": {
                    select:{
                        url: true
                    }
                },
                "wishlist": {
                    select:{
                        id: true
                    }
                }
            }
        });

        return  {
                    "status": 200,
                    "product_category_detail": post_process_list_detail(product_category_detail)
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const productsService = async () => {
    try {
        const product = await prisma.product.findMany({
            select: {
                "name": true,
                "description": true,
                "product_category": true,
            }
        });
        return  {
                    "status": 200,
                    "product": product
                };
                
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_product_category = (data: any) => {
    let dataArr: Array < {category_name: string;category_image: string} > = [];
    if (data.length > 0) {
        data.forEach((element: any) => {
            dataArr.push({"category_name": element.category_name, "category_image": element.category_image_url});
        });
        return dataArr;
    }
    return {}
}

const post_process_list_detail_ids = (data: any) => {
    let dataArr: Array < any > = [];
    if (data.length > 0) {
        data.forEach((element: any) => {
            dataArr.push(element.id);
        });
        return dataArr;
    } 
    return ''
}
const list_detail_with_wishlist = (data:any, wishlistData: any)=>{
    let dataArr: Array < any > = [];
    if (data.length > 0) {
        data.forEach((element: any) => {
            dataArr.push({
                "id": String(element.product.id),
                "product_variant_id": String(element.id),
                "product_name": element.product.name,
                "description": element.product.description,
                "category": element.product.product_category,
                "shop_name": element.product.shop.name,
                "price": String(element.price),
                "discounted_price": String(element.discounted_price),
                "image": element.product_variant_images[0]?.url,
                "wishlist_id": getWishlistId(wishlistData, element.id),
                "wishlist": compareId(wishlistData, element.id)
            });
        });
        return dataArr;
    } 
}
const getWishlistId = (wishlistData: any, productVariantId:number|bigint) => {
    if(wishlistData !== null && wishlistData.length !== undefined && wishlistData.length !== 0){
        const filterResult = wishlistData.filter((element:any)=> element.product_variant_id === productVariantId);
        if(filterResult.length > 0) {
            return String(filterResult[0].id);
        }else{
            return null
        }
    }else{
        return null
    }
}
const compareId = (wishlistData: any, productVariantId:number|bigint) => {
    if(wishlistData !== null && wishlistData.length !== undefined && wishlistData.length !== 0){
        const filterResult = wishlistData.filter((element:any)=> element.product_variant_id === productVariantId);
        if(filterResult.length > 0) {
            return true
        }else{
            return false
        }
    }else{
        return false
    }
}

const post_process_list_detail = (data: any) => {
    let dataArr: Array < any > = [];
    if (data.length > 0) {
        data.forEach((element: any) => {
            dataArr.push({
                "id": String(element.product.id),
                "product_variant_id": String(element.id),
                "product_name": element.product.name,
                "description": element.product.description,
                "category": element.product.product_category,
                "shop_name": element.product.shop.name,
                "price": String(element.price),
                "discounted_price": String(element.discounted_price),
                "image": element.product_variant_images[0]?.url
            });
        });
        return dataArr;
    } 
}

const post_process_product_list_detail = (data: any) => {
    let dataArr: Array < any > = [];
    if (data.length > 0) {
        data.forEach((element: any) => {
            dataArr.push({
                "id": String(element.id),
                "product_variant_id": String(element.product_variants[0].id),
                "product_name": element.name,
                "description": element.description,
                "category": element.product_category,
                "shop_name": element.shop.name,
                "price": String(element.product_variants[0].price),
                "discounted_price": String(element.product_variants[0].discounted_price),
                "image": element.product_variants[0].product_variant_images[0]?.url,
                "wishlist_id": String(element.product_variants[0].wishlist[0]?.id !== undefined ? element.product_variants[0].wishlist[0]?.id : undefined)
            });
        });
        return dataArr;
    } 
}
const post_process_product =(productData: any) =>{
    let product_result: productResponseDto = < productResponseDto > {};
    product_result['product'] = getProductInfo(productData);
    let product_variant = convertBigInt2String(productData['product_variants']);
    product_variant.forEach((e:any) => {e['size'] = `${e.measurement} ${e.measurement_unit}`});
    product_result['product_variants'] = getProductVariantsInfo(product_variant);
    return product_result;
}
const getProductInfo = (productData:any) => {
    let product_result: productInfoDto = < productInfoDto > {};
    product_result['product_name'] = productData['name'];
    product_result['description'] = productData['description'];
    product_result['category'] = productData['product_category'];
    product_result['shop_name'] = productData['shop']['name'];
    
    let product_variant = convertBigInt2String(productData['product_variants']);
    product_variant.forEach((e:any) => {e['size'] = `${e.measurement} ${e.measurement_unit}`});
    product_result['colors_available'] = getColorsInfo(product_variant);
    product_result['sizes_available'] = getSizesInfo(product_variant);
    return product_result;
}

const getProductVariantsInfo = (product_variants: any) => {
    const unique_variants = [...new Set(product_variants.map((item: any) => item.id))]
    let product_variants_available: any = {};
    unique_variants.forEach((e: any) => {product_variants_available[e] = {}})
    product_variants.forEach((variant: any) => {
        product_variants_available[variant.id]["price"] = variant.price;
        product_variants_available[variant.id]["sku"] = variant.sku;
        product_variants_available[variant.id]["color"] = variant.color;
        product_variants_available[variant.id]["measurements"] = variant.size;
        product_variants_available[variant.id]["discounted_price"] = variant.discounted_price;
        product_variants_available[variant.id]["country_of_origin"] = variant.country_of_origin;
        product_variants_available[variant.id]["images"] = get_images_array(variant.product_variant_images);
        product_variants_available[variant.id]["videos"] = get_videos_array(variant.product_variant_images);

    })

    return product_variants_available;
}
const getColorsInfo = (product_variants: any) => {
    const unique_colors = [...new Set(product_variants.map((item:any) => item.color))];
    let colors_available:any = {};
    unique_colors.forEach((e: any) => {colors_available[e]={}})
    product_variants.forEach((variant:any) => {
        colors_available[variant.color][variant.size] = variant.id;
    });
    return colors_available;
}


const getSizesInfo = (product_variants: any) => {
    const unique_size = [...new Set(product_variants.map((item:any) => item.size))];
    let size_available:any = {};
    unique_size.forEach((e: any) => {size_available[e]={}})
    product_variants.forEach((variant:any) => {
        size_available[variant.size][variant.color] = variant.id;
    });
    return size_available;
}

const get_images_array = (images: any) => {
    let imagesArr: Array<string> = [];
    if(images.length > 0) {
        images.forEach((element: any)=>{
            imagesArr.push(element.url)
        })
        return imagesArr;
    }
    return [];
} 

const get_videos_array = (videos: any) => {
    let videosArr: Array<string> = [];
    if(videos.length > 0) {
        videos.forEach((element: any)=>{
            videosArr.push(element.url)
        })
        return videosArr;
    }
    return [];
} 
