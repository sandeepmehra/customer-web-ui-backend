import prisma from "../../../prisma/prisma-client";
import {
    userAddressDto,
    userAddressResponseDto,
    userPutAddressDto
} from '../dtos/user.dto';
import { wishlistDbDto, wishlistResponseDto } from "../dtos/wishlist.dto";
import { convertBigInt2String } from "../helpers/convert.helper";
import { generateAccessToken, generateRefreshToken } from "../helpers/jwt.helper";
require('dotenv').config();

export const basicInfoService = async (userDto: {
    name: string;email: string;mobile_number: string
}, email ? : string, mobile_number ? : string ) => {
    try {
        if(email !== undefined){
            const user = await prisma.user_customer.update({
                where: {
                    "email": email
                },
                data: {
                    "name": userDto.name,
                    "email": userDto.email,
                    "mobile_number": userDto.mobile_number,
                },
                select:{
                    "name": true,
                    "email": true
                }
            });
            return {
                access_token: generateAccessToken(user.name as string, user.email as string),
                refresh_token: generateRefreshToken(user.name as string, user.email as string)
            };
        }else{
            const user = await prisma.user_customer.update({
                where: {
                    "mobile_number": mobile_number
                },
                data: {
                    "name": userDto.name,
                    "email": userDto.email,
                    "mobile_number": userDto.mobile_number
                }
            });
            return {
                access_token: generateAccessToken(user.name as string, user.mobile_number as string),
                refresh_token: generateRefreshToken(user.name as string, user.mobile_number as string)
            };
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const getUserInfoService = async (email ? : string, mobile_number ? : string) => {
    try {
        let userInfo;
        if(email !== undefined){
            userInfo = await prisma.user_customer.findUnique({
                where: {
                    "email": email
                },
                select: {
                    "name": true,
                    "mobile_number": true,
                    "email": true
                }
            });
        }else {
            userInfo = await prisma.user_customer.findUnique({
                where: {
                    "mobile_number": mobile_number
                },
                select: {
                    "name": true,
                    "mobile_number": true,
                    "email": true
                }
            });
        }

        return {
            "status": 200,
            "userInfo": userInfo
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const getUserAddressService = async (email ? : string, mobile_number ? : string) => {
    try {
        let userAddress;
        if(email !== undefined){
            userAddress = await prisma.customer_order_address.findMany({
                where: {"user_customer": {email : email}},
                orderBy: [{address:{added_on: 'desc'}}],
                select: {
                    "id": true,
                    "name": true,
                    "mobile_number": true,
                    "address": {
                        select: {
                            "city": true,
                            "street": true,
                            "state": true,
                            "pincode": true
                        }
                    }
                }
            });
        }else {
            userAddress = await prisma.customer_order_address.findMany({
                where: {
                    "user_customer": {
                        mobile_number: mobile_number
                    }
                },
                orderBy: [{address:{added_on: 'desc'}}],
                select: {
                    "id": true,
                    "name": true,
                    "mobile_number": true,
                    "address": {
                        select: {
                            "city": true,
                            "street": true,
                            "state": true,
                            "pincode": true
                        }
                    }
                }
    
            });
        }
        return {
            "status": 200,
            "userAddress": post_process_address(convertBigInt2String(userAddress))
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const addressInfoPostService = async (userAddressDto: userAddressDto, email ? : string, mobile_number ? : string) => {
    try {
        if(email !== undefined){
            await prisma.customer_order_address.create({
                data: {
                    "name": userAddressDto.name,
                    "mobile_number": userAddressDto.mobile_number,
                    "address": {
                        create: {
                            "city": userAddressDto.city,
                            "street": userAddressDto.street,
                            "state": userAddressDto.state,
                            "pincode": Number(userAddressDto.pincode)
                        }
                    },
                    "user_customer": {
                        connect: {
                            email: email
                        }
                    }
                }
            });
        }else {
            await prisma.customer_order_address.create({
                data: {
                    "name": userAddressDto.name,
                    "mobile_number": userAddressDto.mobile_number,
                    "address": {
                        create: {
                            "city": userAddressDto.city,
                            "street": userAddressDto.street,
                            "state": userAddressDto.state,
                            "pincode": Number(userAddressDto.pincode)
                        }
                    },
                    "user_customer": {
                        connect: {
                            mobile_number: mobile_number
                        }
                    }
                }
            });
        }
        
        return {
            "status": 201,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const addressInfoUpdateService = async (addressbody: userPutAddressDto, id: number,email ? : string, mobile_number ? : string ) => {
    try {
        if(email !== undefined){
            await prisma.customer_order_address.update({
                where: {
                    "id": Number(id)
                },
                data: {
                    "name": addressbody.name,
                    "mobile_number": addressbody.mobile_number,
                    "address": {
                        update: {
                            "city": addressbody.city,
                            "street": addressbody.street,
                            "state": addressbody.state,
                            "pincode": addressbody.pincode
                        }
                    }  
                }
            })
        }else{
            await prisma.customer_order_address.update({
                where: {
                    "id": Number(id)
                },
                data: {
                    "name": addressbody.name,
                    "mobile_number": addressbody.mobile_number,
                    "address": {
                        update: {
                            "city": addressbody.city,
                            "street": addressbody.street,
                            "state": addressbody.state,
                            "pincode": addressbody.pincode
                        }
                    }
                }
            })
        }
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const getWishlistService = async (email ? : string, mobile_number ? : string) => {
    try {
        let wishlist;
        if(email !== undefined){
            wishlist = await prisma.wishlist.findMany({
                where:{ customer:{email: email}
                },
                orderBy: [{
                    'added_on': 'desc'
                }],
                select: {
                    id: true,
                    product_variant: {
                        select: {
                            id: true,
                            price: true,
                            discounted_price: true,
                            product_variant_images:{
                                select: {
                                    url: true
                                }
                            },
                            product: {
                                select: {
                                    name: true,
                                    description: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }else {
            wishlist = await prisma.wishlist.findMany({
                where:{ customer:{mobile_number: mobile_number}
                },
                orderBy: [{
                    'added_on': 'desc'
                }],
                select: {
                    id: true,
                    product_variant: {
                        select: {
                            id: true,
                            price: true,
                            discounted_price: true,
                            product_variant_images:{
                                select: {
                                    url: true
                                }
                            },
                            product: {
                                select: {
                                    name: true,
                                    description: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        return {
            "status": 200,
            "wishlist": post_process_wishlist(wishlist)
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const addToWishlistService = async (product_variant_id: number, email ? : string, mobile_number ? : string) => {
    try {
        if(email !== undefined){
            const productRes = await prisma.product_variant.findUnique({
                    where:{id: product_variant_id}
            });
            if(productRes === undefined && productRes === null){
                return {
                    status: 400,
                    message: "product information are not exists"
                }
            }
                
            const customerId:any = await prisma.user_customer.findUnique({
                where:{email: email},
                select: {
                    id: true
                }
            });
            let productAlreadyExists:any;
            if(customerId !== undefined && customerId !== null && product_variant_id){
                productAlreadyExists = await prisma.wishlist.findMany({
                    where:{AND:[{product_variant: {id: product_variant_id}},{customer:{id: Number(customerId.id)}}]}
                });
            }
            if(productAlreadyExists[0] !== undefined){
                return {
                    status: 400,
                    message: 'Product is already exists in the wishlist'
                };
            }
            await prisma.wishlist.create({
                data: {
                    "product_variant": {
                        connect: {id: product_variant_id}
                    }, 
                    "customer": {
                        connect: {email: email}
                    }
                }
            });
        }else {
            const productRes = await prisma.product_variant.findUnique({
                where:{id: product_variant_id}
            });
            if(productRes === undefined && productRes === null){
                return {
                    status: 400,
                    message: "product information are not exists"
                }
            }
            
            const customerId:any = await prisma.user_customer.findUnique({
                where:{mobile_number: mobile_number},
                select: {
                    id: true
                }
            });
            let productAlreadyExists:any;
            if(customerId !== undefined && customerId !== null && product_variant_id){
                productAlreadyExists = await prisma.cart_item.findMany({
                    where:{AND:[{product_variant: {id: product_variant_id}},{customer:{id: Number(customerId.id)}}]}
                });
            }
            if(productAlreadyExists[0] !== undefined){
                return {
                    status: 400,
                    message: 'Product is already exists in the wishlist'
                };
            }
            await prisma.wishlist.create({
                data: {
                    "product_variant": {
                        connect: {id: product_variant_id}
                    }, 
                    "customer": {
                        connect: {mobile_number: mobile_number}
                    }
                }
            });
        }
        
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const removeToWishlistService = async (wishlist_id: number, email ? : string, mobile_number ? : string) => {
    try {
        console.log("product_variant_id delete: ", wishlist_id, email);
        if(email !== undefined){
            await prisma.wishlist.delete({where:{id: wishlist_id}});
        }else {
            await prisma.wishlist.delete({where:{id: wishlist_id}});
        }
        
        return {
            "status": 204,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_address = (addressess: any) => {
    let addressArr: Array < userAddressResponseDto > = [];
    if (addressess.length > 0) {
        addressess.forEach((element: any) => {
            addressArr.push({
                "id": element.id,
                "name": element.name,
                "mobile_number": String(element.mobile_number),
                "street": element.address.street,
                "city": element.address.city,
                "state": element.address.state,
                "pincode": String(element.address.pincode)
            });
        });
        return addressArr;
    }
};
const post_process_wishlist = (wishlistRes: any) => {
    let wishlistArr: Array < wishlistResponseDto > = [];
    if (wishlistRes.length > 0) {
        wishlistRes.forEach((element: wishlistDbDto) => {
            wishlistArr.push({
                "id": String(element.id),
                "product_variant_id": String(element.product_variant.id),
                "product_name": element.product_variant.product.name,
                "description": element.product_variant.product.description,
                "shop_name": element.product_variant.product.shop.name,
                "price": element.product_variant.price,
                "discounted_price": element.product_variant.discounted_price,
                "image": element.product_variant.product_variant_images.length > 0 ? element.product_variant.product_variant_images[0].url : '',
            });
        });
        return wishlistArr;
    }
}