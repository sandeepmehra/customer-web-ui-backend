import prisma from "../../../prisma/prisma-client";
require('dotenv').config();
import { createClient } from '@supabase/supabase-js';
import formidable from 'formidable';
import fs from "fs";
const supabase = createClient(process.env.SUPABASE_STORAGE_BASE_URL as string, process.env.SUPABASE_STORAGE_BASE_ANON_KEY as string)

export const postRatingReviewService = async (order_item_id: number, reviewDto:{review:string; rating:number;}, email ? : string, mobile_number ? : string) => {
    try {
        if (email !== undefined) {
            if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{email: email}}}]}}) === 1){
                await prisma.order_item_review.create({
                    data: {
                        "review": reviewDto.review,
                        "rating": reviewDto.rating,
                        order_item: {
                            connect: {id: order_item_id}
                        }
                    }
                });
            }
            
        }else{
            if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{mobile_number: mobile_number}}}]}}) === 1){
                await prisma.order_item_review.create({
                    data: {
                        "review": reviewDto.review,
                        "rating": reviewDto.rating,
                        order_item: {
                            connect: {id: order_item_id}
                        }
                    }
                });
            }
        }   
        return {
            "status": 201,
            "messsage": {}
        };        
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const postImageService = async (order_item_id: number, req: any, res:any, email ? : string, mobile_number ? : string) => {
    const parseForm = (req:any) => {
        const form = new formidable.IncomingForm({multiples: true, maxFieldsSize: 10*1024*1024});
        return new Promise(
            function (resolve, reject) {
                form.parse(req, (err, fields, files) => {
                if (err) reject(err);
                else resolve([files]);
                })
        })
    }
        
    let files:any = await parseForm(req);
    if(files[0].image_file === undefined){
        return res.status(400).json({message:"Files does not contain Images."})
    }
    if(files[0].image_file.length > 0){
        let imageStoragePath: Array<string> = [];
        await Promise.all(files[0].image_file.map(async(item: any)=>{
            let filepath = `${item.newFilename}-${item.originalFilename}`;
            filepath = filepath.replace(/\s/g, "-");
            const rawData = fs.readFileSync(item.filepath);
            const { data, error } = await supabase.storage
                .from(process.env.IMAGE_BUCKET_NAME as string)
                .upload('order-review-gallary/'+ filepath, rawData, {contentType: item.mimetype});
            if(error){
                return res.status(400).json(error);
            }              
            imageStoragePath.push(data?.Key as string);
        }));
        try { 
            if (email !== undefined) {
                if(imageStoragePath.length > 0){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    imageStoragePath.forEach((url: string) => {
                        url_data.push({
                            order_item_id: Number(order_item_id),
                            url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+url
                        })
                    });
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    return res.status(201).json({});
                }else{
                    return res.status(400).json({message: 'Files does not contain Images.'})
                }
            }            
            else{
                if(imageStoragePath.length > 0){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    imageStoragePath.forEach((url: string) => {
                        url_data.push({
                            order_item_id: Number(order_item_id),
                            url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+url
                        })
                    });
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    return res.status(201).json({});
                }else{
                    return res.status(400).json({message: 'Files does not contain Images.'}) 
                }
            }                             
                     
        } catch (error:any) {
            return {
                "status": 500,
                "message": error.message
            } 
        }
            
    }
}

function formidablePromise(req:any) {
    return new Promise(((resolve, reject) => {
        const form:any = new formidable.IncomingForm({multiples: true});
        form.parse(req, (err:any, fields:any, files:any) => {
            if (err) return reject(err);
            resolve({ fields, files });
        });
    }));
} 
export const postRatingReviewImageService = async (order_item_id: number, req: any, res:any, email ? : string, mobile_number ? : string) => {
    async function createReviewOrRating(formParseData:any){
        
            await prisma.order_item_review.create({
                data: {
                    "review": formParseData.fields.review,
                    "rating": Number(formParseData.fields.rating),
                    "action_taken_on": new Date(),
                    "action_taken": false,
                    order_item: {
                        connect: {id: order_item_id}
                    }
                }
            });
            return res.status(201).json({});
    }
    try {
        if (email !== undefined) {
            const formParseData:any = await formidablePromise(req); //Got files, and fields
            if(Object.keys(formParseData.files).length === 0 && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{email: email}}}]}}) === 1){
                    createReviewOrRating(formParseData);
                }
            }
            if(formParseData.files !== undefined && formParseData.files.files !== undefined && formParseData.files.files.length === undefined){
                let filepath = `${formParseData.files.files.newFilename}-${formParseData.files.files.originalFilename}`;
                filepath = filepath.replace(/\s/g, "-");
                const rawData = fs.readFileSync(formParseData.files.files.filepath);        
                const { data, error } = await supabase.storage
                        .from(process.env.IMAGE_BUCKET_NAME as string)
                        .upload('order-review-gallary/'+ filepath, rawData, {contentType: formParseData.files.files.mimetype});
                if(error){
                    return res.status(400).json(error);
                }
                console.log("data.Key", data);
                if(data !== null && Object.keys(data).length !== 0 && data?.Key !== undefined && data?.Key !== null){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    url_data.push({
                        order_item_id: Number(order_item_id),
                        url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+data?.Key
                    })
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    if(formParseData !== undefined && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                        if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{email: email}}}]}}) === 1){
                            createReviewOrRating(formParseData);
                        }
                    }
                }
            }

            if(formParseData.files !== undefined && formParseData.files.files !== undefined &&  formParseData.files.files.length > 1){
                const response:any = await Promise.allSettled(formParseData.files.files.map(async(item: any)=>{
                    let filepath = `${item.newFilename}-${item.originalFilename}`;
                    filepath = filepath.replace(/\s/g, "-");
                    const rawData = fs.readFileSync(item.filepath);
                    const { data, error } = await supabase.storage
                        .from(process.env.IMAGE_BUCKET_NAME as string)
                        .upload('order-review-gallary/'+ filepath, rawData, {contentType: item.mimetype});
                        if(error){
                            return res.status(400).json(error);
                        } 
                        return data?.Key as string
                }));
                if(response.length > 0){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    response.forEach((item: {status: string; value: string}) => {
                        if(item.status === 'fulfilled'){
                            url_data.push({
                                order_item_id: Number(order_item_id),
                                url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+item.value
                            })
                        }
                    });
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    if(formParseData !== undefined && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                        if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{email: email}}}]}}) === 1){
                            createReviewOrRating(formParseData);
                        }
                    }
                }
            }
        }else{
            const formParseData:any = await formidablePromise(req); //Got files, and fields
            if(Object.keys(formParseData.files).length === 0 && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{mobile_number: mobile_number}}}]}}) === 1){
                    createReviewOrRating(formParseData);
                }
            }
            if(formParseData.files !== undefined && formParseData.files.files !== undefined && formParseData.files.files.length === undefined){
                let filepath = `${formParseData.files.files.newFilename}-${formParseData.files.files.originalFilename}`;
                filepath = filepath.replace(/\s/g, "-");
                const rawData = fs.readFileSync(formParseData.files.files.filepath);        
                const { data, error } = await supabase.storage
                        .from(process.env.IMAGE_BUCKET_NAME as string)
                        .upload('order-review-gallary/'+ filepath, rawData, {contentType: formParseData.files.files.mimetype});
                if(error){
                    return res.status(400).json(error);
                }
                console.log("data.Key", data);
                if(data !== null && Object.keys(data).length !== 0 && data?.Key !== undefined && data?.Key !== null){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    url_data.push({
                        order_item_id: Number(order_item_id),
                        url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+data?.Key
                    })
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    if(formParseData !== undefined && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                        if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{mobile_number: mobile_number}}}]}}) === 1){
                            createReviewOrRating(formParseData);
                        }
                    }
                }
            }

            if(formParseData.files !== undefined && formParseData.files.files !== undefined &&  formParseData.files.files.length > 1){
                const response:any = await Promise.allSettled(formParseData.files.files.map(async(item: any)=>{
                    let filepath = `${item.newFilename}-${item.originalFilename}`;
                    filepath = filepath.replace(/\s/g, "-");
                    const rawData = fs.readFileSync(item.filepath);
                    const { data, error } = await supabase.storage
                        .from(process.env.IMAGE_BUCKET_NAME as string)
                        .upload('order-review-gallary/'+ filepath, rawData, {contentType: item.mimetype});
                        if(error){
                            return res.status(400).json(error);
                        } 
                        return data?.Key as string
                }));
                if(response.length > 0){
                    let url_data : Array<{order_item_id:number; url:string;}> = [];
                    response.forEach((item: {status: string; value: string}) => {
                        if(item.status === 'fulfilled'){
                            url_data.push({
                                order_item_id: Number(order_item_id),
                                url: process.env.PUBLIC_SUPABASE_STORAGE_BASE_URL as string+item.value
                            })
                        }
                    });
                    await prisma.order_item_review_gallary.createMany({
                        data:url_data
                    });
                    if(formParseData !== undefined && formParseData.fields !== undefined && formParseData.fields.review !== undefined && formParseData.fields.rating !== undefined){
                        if (await prisma.order_item.count({where:{AND:[{id: order_item_id},{orders: {customer:{mobile_number: mobile_number}}}]}}) === 1){
                            createReviewOrRating(formParseData);
                        }
                    }
                }
            }
        }         
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}