import prisma from "../../../prisma/prisma-client";
import { cartDbDto, cartResponseDto } from "../dtos/cart.dto";
let util = require("util");

util.inspect.defaultOptions.depth = null
util.inspect.defaultOptions.colors = true
util.inspect.defaultOptions.showHidden = true

export const getCartService = async (email ? : string, mobile_number ? : string) => {
    try {    
        let cartRes:any;
        if (email !== undefined) {
            cartRes= await prisma.cart_item.findMany({
                where:{ customer:{email: email}
                },
                orderBy: [{
                    'added_on': 'desc'
                }],
                select: {
                    id: true,
                    quantity: true,
                    product_variant: {
                        select: {
                            id: true,
                            discounted_price: true,
                            price: true,
                            product_variant_images:{
                                select: {
                                    url: true
                                }
                            },
                            product: {
                                select: {
                                    id: true,
                                    name: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        } else {
            cartRes= await prisma.cart_item.findMany({
                where:{ customer:{mobile_number: mobile_number}
                },
                select: {
                    id: true,
                    quantity: true,
                    product_variant: {
                        select: {
                            id: true,
                            discounted_price: true,
                            product_variant_images:{
                                select: {
                                    url: true
                                }
                            },
                            product: {
                                select: {
                                    id: true,
                                    name: true,
                                    shop:{
                                        select:{ 
                                            name: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        return {
            status: 200,
            cart: post_process_cart(cartRes)
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const postCartService = async (product_variant_id: number, cartDto:{quantity:number}, email ? : string, mobile_number ? : string) => {
    try {
        if (email !== undefined) {
                const productRes:{price:number; product:{shop:{seller:{id:bigint; commission_percentage: number}}}}|null 
                = await prisma.product_variant.findUnique({
                    where:{id: product_variant_id},
                    select: {
                        "price": true,
                        "product": {
                            select: {
                                shop:{
                                    select:{
                                        seller: {
                                            select: {
                                                id: true,
                                                commission_percentage: true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }); 
                if(productRes === undefined && productRes === null){
                    return {
                        status: 400,
                        message: "seller and product information are not exists"
                    }
                }
                if(productRes?.product.shop.seller.id !== undefined){
                    const customerId:any = await prisma.user_customer.findUnique({
                        where:{email: email},
                        select: {
                            id: true
                        }
                    });
                    let productAlreadyExists:any;
                    if(customerId !== undefined && customerId !== null && product_variant_id){
                        productAlreadyExists = await prisma.cart_item.findMany({
                            where:{AND:[{product_variant: {id: product_variant_id}},{customer:{id: Number(customerId.id)}}]}
                        });
                    }
                    if(productAlreadyExists[0] !== undefined && productAlreadyExists.length > 0){
                        return {
                            status: 200,
                            message: 'Product already in the cart.'
                        };
                    }
                    await prisma.cart_item.create({
                        data: {
                            "quantity": Number(cartDto.quantity),
                            // "seller_commission": productRes.product.shop.seller.commission_percentage,
                            "product_variant": {
                                connect: {id: product_variant_id}
                            }, 
                            "customer": {
                                connect: {email: email}
                            },
                            "seller":{
                                connect: {
                                    id: productRes.product.shop.seller.id
                                }
                            }
                        }
                    });
                }
       
        }else{
            const productRes:{price:number; product:{shop:{seller:{id:bigint; commission_percentage: number}}}}|null 
                = await prisma.product_variant.findUnique({
                    where:{id: product_variant_id},
                    select: {
                        "price": true,
                        "product": {
                            select: {
                                shop:{
                                    select:{
                                        seller: {
                                            select: {
                                                id: true,
                                                commission_percentage: true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                if(productRes === undefined && productRes === null){
                    return {
                        status: 400,
                        message: "seller and product information are not exists"
                    }
                }
                if(productRes?.product.shop.seller.id !== undefined){

                    const customerId:any = await prisma.user_customer.findUnique({
                        where:{mobile_number: mobile_number},
                        select: {
                            id: true
                        }
                    });
                    let productAlreadyExists:any;
                    if(customerId !== undefined && customerId !== null && product_variant_id){
                        productAlreadyExists = await prisma.cart_item.findMany({
                            where:{AND:[{product_variant: {id: product_variant_id}},{customer:{id: Number(customerId.id)}}]}
                        });
                    }
                    if(productAlreadyExists[0] !== undefined && productAlreadyExists.length > 0){
                        return {
                            status: 400,
                            message: 'Product is already exists in the Cart'
                        };
                    }
                    await prisma.cart_item.create({
                        data: {
                            "quantity": Number(cartDto.quantity),
                            // "seller_commission": productRes.product.shop.seller.commission_percentage,
                            "product_variant": {
                                connect: {id: product_variant_id}
                            }, 
                            "customer": {
                                connect: {mobile_number: mobile_number}
                            },
                            "seller":{
                                connect: {
                                    id: productRes.product.shop.seller.id
                                }
                            }
                        }
                    });
                }
        }
        return {
            "status": 201,
            "messsage": {}
        };
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const putAddCartService = async(product_variant_id:number, email ? : string, mobile_number ? : string) =>{ 
    try {
          if (email !== undefined) {
                const quantity_data = await prisma.cart_item.findMany({
                    where:{AND:[{product_variant: {id: product_variant_id}},{customer:{email: email}}]},
                    select:{id: true, quantity: true, product_variant:{select: {quantity: true}}}
                });
                if(quantity_data[0].quantity == quantity_data[0].product_variant.quantity){
                    return {
                            status: 400,
                            message: "Maximum quantity achieved"
                    }
                }
                else{
                  await prisma.cart_item.updateMany({
                      where: {id: quantity_data[0].id as bigint},
                      data:{quantity: {increment: 1}}
                  });
                  return {
                    status: 204,
                    message: {}
                }
            }
        }else{
            const quantity_data = await prisma.cart_item.findMany({
                where:{AND:[{product_variant: {id: product_variant_id}},{customer:  {mobile_number: mobile_number}}]},
                select:{id: true, quantity: true, product_variant:{select: {quantity: true}}}
            });
    
            if(quantity_data[0].quantity == quantity_data[0].product_variant.quantity){
                return {
                      status: 400,
                      message: "Maximum quantity achieved"
                }
            }
            else{
                await prisma.cart_item.updateMany({
                    where: {id: quantity_data[0].id as bigint},
                    data:{quantity: {increment: 1}}
                });
                return {
                    status: 204,
                    message: {}
                }
            }
        }
        
    } catch (error:any) {
        return {
            "status": 500,
            "message": error.message
        } 
    }
}

export const putSubCartService = async(product_variant_id:number, email ? : string, mobile_number ? : string) =>{
    try {
        if (email !== undefined) {
            await prisma.cart_item.updateMany({
                where:{AND:[{product_variant: {id: product_variant_id}},{customer:  {email: email}}]},
                data:{quantity: {decrement: 1}}
            });    
        }else{
            await prisma.cart_item.updateMany({
                where:{AND:[{product_variant: {id: product_variant_id}},{customer:  {mobile_number: mobile_number}}]},
                data:{quantity: {decrement: 1}}
            });
        }
        await prisma.cart_item.deleteMany({where: {quantity: {equals:0}}});
        return {
            status: 204,
            message: {}
        }
    } catch (error:any) {
        return {
            "status": 500,
            "message": error.message
        } 
    }
}

export const removeCartService = async (cartItemId: number, email ? : string, mobile_number ? : string) => {
    try {
        if(email !== undefined){
            await prisma.cart_item.deleteMany({
                where:{AND:[{id: cartItemId},{customer: {email: email}}]}
            });
        }else{
            await prisma.cart_item.deleteMany({
                where:{AND:[{id: cartItemId},{customer: {mobile_number: mobile_number}}]}
            });
        }
        return {
            "status": 200,
            "messsage": {}
        };
        
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}

const post_process_cart = (cartRes: any) => {
    let cartArr: Array < cartResponseDto > = [];
    if (cartRes.length > 0) {
        cartRes.forEach((element: cartDbDto) => {
            cartArr.push({
                "id": String(element.id),
                "product_variant_id": String(element.product_variant.id),
                "quantity": String(element.quantity),
                "price": String(element.product_variant.price),
                "discounted_price": String(element.product_variant.discounted_price),
                "total_price": post_process_multiply(element.quantity, Number(element.product_variant.price)),
                "product_name": element.product_variant.product.name,
                "product_id": String(element.product_variant.product.id),
                "shop_name": element.product_variant.product.shop.name,
                "image": element.product_variant.product_variant_images.length > 0 ? element.product_variant.product_variant_images[0].url : '',
            });
        });
        return cartArr;
    }
}

const post_process_multiply = (a: number, b: number) => {
    return String(Math.round((a*b) * 100) / 100);
}
