import express from 'express';
import validator from 'validator';
import prisma from "../../../prisma/prisma-client";
import logger from '../../config/logger.config';
import {
    LoginNetworkDto
} from '../dtos/auth.dto';
import {
    generateAccessToken,
    generateRefreshToken,
    verifyJWT
} from "../helpers/jwt.helper";
import {
    generateEmailOTP,
    generateOTP
} from '../helpers/otp.helper';
const Sib = require('sib-api-v3-sdk');
require('dotenv').config();
import axios from 'axios';

export const loginWithEmail = async (userDto: LoginNetworkDto) => {
    const user = await prisma.user_customer.findMany({
        where: {
            AND: [{
                email: {
                    equals: userDto.email
                },
                password: {
                    equals: userDto.password
                }
            }],

        },
        select: {
            name: true,
            suspended: true
        },
    });
    if(user[0].suspended){
        return {
            status: 403,
            message: 'Account suspended'
        };
    }
    if(user[0]) {
        logger('info', 'POST', '/login', 200, `${userDto.email} successfully logged in.`);
        return {
            access_token: generateAccessToken(user[0].name as string, userDto.email),
            refresh_token: generateRefreshToken(user[0].name as string, userDto.email)
        };
    }
    logger('error', 'POST', '/login', 401, `User: ${userDto.email} not found.`);
    return {}
};

export const storeNumberOrOTP = async (mobile_number: string) => {
    while(true){
        try {
            const otp = generateOTP()
            await prisma.user_customer_mobile_otp.create({
                data: {
                    mobile_number: mobile_number,
                    otp: otp
                }
            });
            const data = JSON.stringify({
                "template_name": "customer_login_mobile_otp",
                "broadcast_name": "none",
                "parameters": [
                    {
                    "name": "1",
                    "value": `${otp}`
                    }
                ]
            });
            const config = {
                method: 'post',
                url: `${process.env.WATI_URL}/api/v1/sendTemplateMessage?whatsappNumber=${mobile_number}`,
                headers: { 
                    'Authorization': `${process.env.WATI_TOKEN}`, 
                    'Content-Type': 'application/json'
                },
                data : data
            };
            try {
                const response = await axios(config);
                if(response.data.result === false && response.data.validWhatsAppNumber === false){
                    return {
                        status: 400,
                        data: response.data.info
                    }
                }
                return {
                    status: 201
                }     
            } catch (error:any) {
                return {
                    status: 401
                }
            }
            
        } catch (error: any) {
            console.log(error.response);
            console.log("Re-trying OTP generation");
        }
    }
};

export const otpVerifyService = async (verifyData: {mobile_number:string; otp:string}) => {
    try {
        const {
            mobile_number,
            otp
        } = verifyData;
        const userExists = await prisma.user_customer_mobile_otp.findMany({
            where: {
                AND: [{
                    mobile_number: {
                        equals: mobile_number
                    },
                    otp: {
                        equals: otp
                    },
                    generated_on: {
                        gt: new Date(new Date().getTime() - 15 * 60 * 1000)
                    }
                }],
            },
            select: {
                id: true,
                generated_on: true
            },
        });
        if (userExists.length === 0) {
            return {
                status: 400,
                message: 'Incorrect OTP'
            };
        } else {
            await prisma.user_customer_mobile_otp.delete({
                where: {
                    id: userExists[0].id
                }
            });
            const customerExists = await prisma.user_customer.findUnique({
                where: {
                    mobile_number: String(mobile_number)
                },
                select: {
                    name: true
                }
            });
            if(!customerExists){
                await prisma.user_customer.create({
                    data: {
                        mobile_number: String(mobile_number),
                        name: 'There'
                    }
                });
                return {
                    access_token: generateAccessToken('There', mobile_number),
                    refresh_token: generateRefreshToken('There', mobile_number)
                };
            }

            logger('info', 'POST', '/mobile', 200, `${mobile_number} successfully logged in.`);
            return {
                access_token: generateAccessToken(customerExists.name as string, mobile_number),
                refresh_token: generateRefreshToken(customerExists.name as string, mobile_number)
            };
        }
       
    } catch (error: any) {
        console.log(error);
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const storeEmailOrOTP =  async (email: string) => {
    while(true){
        try {
            const otp = generateEmailOTP();
            await prisma.user_customer_email_otp.create({
                data: {
                    email: email,
                    otp: otp
                }
            });
           
            const client = Sib.ApiClient.instance
            const apiKey = client.authentications['api-key']
            apiKey.apiKey = process.env.EMAIL_API_KEY
            const tranEmailApi = new Sib.TransactionalEmailsApi()
            const sender = {
                email: 'sandysmart017@gmail.com',
                name: 'Sandeep Singh Mehra',
            }
            const receivers = [
                {
                    email: email, 
                },
            ]
            try {
                await tranEmailApi.sendTransacEmail({
                        sender,
                        to: receivers,
                        subject: 'Urban Plant Verification Code',
                        textContent: `
                        Urban Plant Verification Code
                        `,
                        htmlContent: `
                            <div>
                                <p>Hi,</p>
                                <p>We received a request to access your Urban Plant Account {{params.email}} through your email address.</p>
                                <p>Your Urban Plant verification OTP is: <h1 style="text-align: center;">{{params.otp}}</h1></p>
                            </div>
                        `,
                        params: {
                            email: email,
                            otp: otp,
                        },
                });
                return {
                    status: 201,
                    data: {}
                }     
            } catch (error) {
                return {
                    status: 401
                }
            }
            
        } catch (error: any) {
            console.log(error);
            console.log("Re-trying OTP generation");
        }
    }
};

export const otpEmailVerifyService = async (verifyData: {email:string; password:string; otp:string}) => {
    try {
        const {
            email,
            password,
            otp
        } = verifyData;
        const userExists = await prisma.user_customer_email_otp.findMany({
            where: {
                AND: [{
                    email: {
                        equals: email
                    },
                    otp: {
                        equals: otp
                    },
                    generated_on: {
                        gt: new Date(new Date().getTime() - 15 * 60 * 1000)
                    }
                }],
            },
            select: {
                id: true,
                generated_on: true
            },
        });
        if (userExists.length === 0) {
            return {
                status: 400,
                message: 'Incorrect OTP'
            };
        } else {
            await prisma.user_customer_email_otp.delete({
                where: {
                    id: userExists[0].id
                }
            });
            const customerExists = await prisma.user_customer.findUnique({
                where: {
                    email: email
                },
                select: {
                    name: true
                }
            });
            if(!customerExists){
                return {
                    status: 400,
                    "message": "Invalid user email"
                };
            }
            await prisma.user_customer.update({
                where:{
                    email: email
                }, 
                data:{
                    password: password
                }
            });
            return {
                status: 200
            }
        }
       
    } catch (error: any) {
        console.log(error);
        return {
            "status": 500,
            "message": error.message
        }
    }
}

export const passwordResetService = async (dto: {password: string}, email ? : string, mobile_number ? : string) => {
        try {
            if(email !== undefined){
                await prisma.user_customer.update({
                    where: {
                        "email": email
                    },
                    data: {
                        "password": dto.password
                    }
                })
            }else{
                await prisma.user_customer.update({
                    where: {
                        "mobile_number": mobile_number
                    },
                    data: {
                        "password": dto.password
                    }
                })
            }
        return {
            "status": 204
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const signupWithEmail = async (userDto: LoginNetworkDto) => {
    try {
        console.log("signup with Email servie", userDto);
        await prisma.user_customer.create({
            data: {
                name: userDto.name,
                email: userDto.email,
                password: userDto.password
            }
        });
        return {
            "status": 201,
            "message": {}
        }
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


export const refresh = async (req: express.Request, res: express.Response) => {
    const access_token = req.headers.authorization !== undefined ? req.headers.authorization : '';
    verifyJWT(req.body['refresh_token'], '/refresh', 'POST', 'refresh', res, (rt_name: string, rt_sub: string) => {
        verifyJWT(access_token, '/refresh', 'POST', 'access', res, async(at_name: string, at_sub: string) => {
            if(at_name === rt_name && at_sub === rt_sub) {
                if (validator.isEmail(rt_sub)){
                    req.email = rt_sub;
                }
                else{
                    req.mobile = rt_sub;
                }
                // database call to verify email/mobile_number and name
                // count findUnique user_customer table 
                let userExists;
                if(req.email !== undefined){
                    userExists = await prisma.user_customer.count({
                        where: {
                            AND:[{email: req.email}, { name: rt_name}]   
                        }
                    });
                }else{
                    userExists = await prisma.user_customer.count({
                        where: {
                            AND:[{mobile_number: req.mobile}, { name: rt_name}]
                        }
                    });
                }
                if(userExists === 0){
                    logger('error', 'POST', '/refresh', 401, 'Error in refresh token');
                    return res.status(401).json({
                        "message": process.env.RESPONSE_UNAUTHORIZED
                    });
                }
                return res.status(200).setHeader('Authorization', generateAccessToken(at_name, at_sub)).json({});
            } else {
                logger('error', 'POST', '/refresh', 401, 'Different "name" and "sub" claims in access token and refresh token');
                return res.status(401).json({
                    "message": process.env.RESPONSE_UNAUTHORIZED
                });
            }
        }, true);
    });
};


export const googleLoginService = async(user: any) => {
    console.log("user in googleLoginService", user, user.name, user.access_token);
    try{
        const config = {
            method: 'get',
            url: `https://oauth2.googleapis.com/tokeninfo?access_token=${user.access_token}`
        };
        const {data} = await axios(config);
        if(!data.email && (Number(data.expires_in) === 0) && data.email_verified !== 'true'){
            return {
                status: 401,
                message: 'Token Invalid'
            }
        }
        const customerExists = await prisma.user_customer.findUnique({
            where: {
                email: data.email
            }
        })
        if(!customerExists){
            await prisma.user_customer.create({
                data: {
                    email: data.email,
                    name: user.name
                }
            })
        }
        return {
            at: generateAccessToken(user.name, data.email),
            rt: generateRefreshToken(user.name, data.email)
        } 
    } catch (error: any) {
        return {
            "status": 500,
            "message": error.message
        }
    }
}


