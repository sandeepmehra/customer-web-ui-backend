import validator from 'validator';
import {
    NextFunction,
    Request,
    Response
} from "express";
import logger from "../../config/logger.config";
import {
    verifyJWT
} from "../helpers/jwt.helper";
import {
    isAuthHeaderValid,
} from "../validations/auth.validation";
import prisma from '../../../prisma/prisma-client';

require('dotenv').config();

//isAccountSuspended function
export const isAccountSuspended = async (email:string, mobile:string): Promise < {
    status: boolean,
    status_code: number,
    message: string | undefined,
    log_message: string
} > =>{
    // console.log("email isAccountSuspended: ", email);
    try {
        let isUserSuspended:{suspended: boolean | null} | null;
        if(email !== 'undefined'){
            isUserSuspended = await prisma.user_customer.findUnique({
                where:{
                    email: email
                },
                select: {
                    suspended: true
                }
            });
        }else{
            isUserSuspended = await prisma.user_customer.findUnique({
                where:{
                    mobile_number: mobile
                },
                select: {
                    suspended: true
                }
            });
        }
        return {
            status_code: isUserSuspended !== null && isUserSuspended !== undefined && isUserSuspended.suspended ? 403 : 200,
            status: isUserSuspended !== null && isUserSuspended !== undefined && isUserSuspended.suspended ? true : false,
            message: isUserSuspended !== null && isUserSuspended !== undefined && isUserSuspended.suspended ? 'Account suspended': 'Access allowed',
            log_message: isUserSuspended !== null && isUserSuspended !== undefined && isUserSuspended.suspended ? 'Accound suspended': 'Access allowed',
        };
    } catch (error: any) {
        return {
            status_code: 500,
            status: false,
            message: process.env.RESPONSE_INTERNAL_SERVER_ERROR,
            log_message: 'Accound suspended',
        };
    }
}

export const accessTokenProtectedVerificationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (isAuthHeaderValid(req.headers.authorization))
        verifyJWT(req.headers.authorization !== undefined ? req.headers.authorization.split(' ')[1] : '', req.path, req.method, 'access', res, async (name: string, sub: string) => {
            if (validator.isEmail(sub)) {
                //suspended or non-suspended status check
                //if(suspended ) return 403 message: Account suspended
                const val_data = await isAccountSuspended(sub, 'undefined');
                if(val_data.status === true && val_data.status_code === 403){
                    return res.status(403).json({message: val_data.message});
                }
                req.email = sub;
            } else {
                const val_data = await isAccountSuspended('undefined', sub);
                if(val_data.status === true && val_data.status_code === 403){
                    return res.status(403).json({message: val_data.message});
                }
                req.mobile = sub;
            }
            next();
        });
    else{
        req.email = '';
        req.mobile = '';
        next();
    }
};


export const accessTokenPrivateVerificationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (isAuthHeaderValid(req.headers.authorization))
        verifyJWT(req.headers.authorization !== undefined ? req.headers.authorization.split(' ')[1] : '', req.path, req.method, 'access', res, async (name: string, sub: string) => {
            if (validator.isEmail(sub)) {
                const val_data = await isAccountSuspended(sub, 'undefined');
                if(val_data.status === true && val_data.status_code === 403){
                    return res.status(403).json({message: val_data.message});
                }
                req.email = sub;
            } else {
                const val_data = await isAccountSuspended('undefined', sub);
                if(val_data.status === true && val_data.status_code === 403){
                    return res.status(403).json({message: val_data.message});
                }
                req.mobile = sub;
            }
            next();
        });
    else{
        logger('error', req.method, req.path, 401, 'Invalid access token');
        return res.status(401).json({
            "message": process.env.RESPONSE_UNAUTHORIZED
        });
    }
};
