import passport from "passport";
import passportGoogle from "passport-google-oauth20";
require('dotenv').config();
const GoogleStrategy = passportGoogle.Strategy;
const clientIDValue:any = process.env.GOOGLE_CLIENT_ID;
const clientSecretValue: any = process.env.GOOGLE_CLIENT_SECRET;

passport.use(
  new GoogleStrategy(
    {
      clientID: clientIDValue,
      clientSecret: clientSecretValue,
      callbackURL: "/auth/google/callback",
      passReqToCallback: true
    },
  async (req, token, refreshToken, profile, done) => {

    try {
      console.log({name: profile.displayName, access_token: token});
      done(null, {name: profile.displayName, access_token: token});
    } catch (err) {
      console.error(err)
    }
      
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user: any, done) => {
  done(null, user);
});
