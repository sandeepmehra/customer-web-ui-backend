
export const generateOTP = () => {
    const digits = '0123456789';
    let otp = '';
    for (let i = 0; i < 4; i++ ) {
        otp += digits[Math.floor(Math.random() * 10)];
    }
    return otp;
}
export const generateEmailOTP = () => {
    const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let otp = '';
    for (let i = 0; i < 4; i++ ) {
        otp += letters[Math.floor(Math.random() * letters.length)];
    }
    return otp;
}