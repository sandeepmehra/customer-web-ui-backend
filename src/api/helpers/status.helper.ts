export const get_order_status = (order_items: any) => {
    let orderItemStatuses: Array < any > = [];
    if (order_items.length > 0) {
        order_items.forEach((element: any) => {
            orderItemStatuses.push(element.status);
        });

        if (orderItemStatuses.every(e => e == 'Rejected'))
            return "Rejected";

        const newArrStatuses: Array < string > = orderItemStatuses.filter(e => e != 'Rejected')

        if (orderItemStatuses.length > 0) {
            if (newArrStatuses.every(e => e == 'Delivered'))
                return "Delivered"

            if (newArrStatuses.some(e => e == 'No-Action' || e == 'Preparing'))
                return "Placed";

            return "Shipped"

        }
    }
}
export const res_order_status = (order_item_status: any) => {
        if ( order_item_status == 'Rejected')
            return "Rejected";
        if (order_item_status == 'Delivered')
            return "Delivered"

        if (order_item_status == 'No-Action' || order_item_status == 'Preparing')
                return "Placed";

        return "Shipped"

}
