import { BigIntInterface } from "../dtos/convert.dto";

export function convertBigInt2String(data: Array<BigIntInterface>) {
    return data.map((e) => {
        Object.keys(e).forEach((k) => {
            if (typeof e[k] === 'bigint') {
                e[k] = String((e[k]));
            }
        });
        return e;
    })
}