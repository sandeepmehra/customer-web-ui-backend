import express from "express";
import * as http from 'http';
require('dotenv').config();
import helmet from "helmet";
import cors from 'cors';
import logger from "./config/logger.config";
import passport from "passport";
const bodyParser=require('body-parser')

const session = require("express-session");
require("./api/helpers/passport");
import {
    RoutesConfig
} from './config/routes.config';
import {
    AuthRoutes,
    UserRoutes,
    ComplaintRoutes,
    ProductRoutes,
    RatingRoutes,
    CartRoutes,
    OrderRoutes
} from "./api/routes/all.routes"

const app: express.Application = express();
const server: http.Server = http.createServer(app);

app.use(helmet());

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(session({
    resave:false,
    saveUninitialized: false,
    secret: ('hush'),
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(__dirname + '/public'));
app.use(
    cors({
        credentials: true,
        allowedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token', 'access-control-allow-credentials'],
        exposedHeaders: ['Authorization','Origin','X-Requested-With','Content-Type','Accept','X-Access-Token','access-control-allow-credentials'],
        methods: 'GET,PUT,POST,DELETE,OPTIONS,HEAD,PATCH',
        origin: `${process.env.UI_URL}`,
        preflightContinue: false,
        optionsSuccessStatus: 200,
    })
);

const routes: Array < RoutesConfig > = [
    new AuthRoutes(app),
    new UserRoutes(app), 
    new ComplaintRoutes(app),
    new ProductRoutes(app),
    new RatingRoutes(app),
    new CartRoutes(app),
    new OrderRoutes(app),
];

const runningMessage = `Server running at http://localhost:${process.env.PORT}`;
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(runningMessage);
});

const isEnvValid = () => {
    const envs = [
        'PORT', 'RESPONSE_UNAUTHORIZED', 'RESPONSE_FORBIDDEN', 'RESPONSE_NOT_FOUND', 'RESPONSE_INTERNAL_SERVER_ERROR', 'ACCESS_TOKEN_SECRET', 'REFRESH_TOKEN_SECRET', 'STAGE', 'DATABASE_URL'
    ];
    envs.forEach((e: string, i: number) => {
        if (process.env[e] === undefined){
            logger('error', 'GET', '/', 500, `Environment Variable: "${e}" is missing`);
            return ;
        }
        if (envs.length === i+1) return runServer();
    });
}

const runServer = () =>{
    server.listen(process.env.PORT, () => {
        routes.forEach((route: RoutesConfig) => {
            logger('info', 'GET', '/', 200, route.getName());
        });
        console.log(runningMessage);
    });
}
isEnvValid();