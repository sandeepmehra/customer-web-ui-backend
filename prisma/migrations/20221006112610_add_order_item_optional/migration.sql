-- AlterTable
ALTER TABLE "order_item" ALTER COLUMN "seller_commission" DROP NOT NULL,
ALTER COLUMN "price_per_unit" DROP NOT NULL,
ALTER COLUMN "status" DROP NOT NULL,
ALTER COLUMN "dispatch_date" DROP NOT NULL,
ALTER COLUMN "delivery_date" DROP NOT NULL;
