/*
  Warnings:

  - You are about to drop the column `address_id` on the `user_customer` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "user_customer" DROP CONSTRAINT "user_customer_address_id_fkey";

-- DropIndex
DROP INDEX "user_customer_address_id_key";

-- AlterTable
ALTER TABLE "user_customer" DROP COLUMN "address_id";
