/*
  Warnings:

  - Added the required column `updated_on` to the `product_variant` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "product_variant" ADD COLUMN     "updated_on" TEXT NOT NULL;
